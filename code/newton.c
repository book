#include "mandelbrot.h"
#include "hsv2rgb.h"
#include "pi.h"

#define width 1280
#define height 720
#define maximum_iterations 12

complex long double n[height][width];

int main() {
  memset(n, 0, sizeof(n[0][0]) * height * width);
  for (int k = 0; k <= maximum_iterations; ++k) {
    #pragma omp parallel for
    for (int j = 0; j < height; ++j) {
      for (int i = 0; i < width; ++i) {
        complex long double c = coordinatel(i, j, width, height, -0.75f, 1.25f);
        n[j][i] = nucleusl(c, k, 256);
      }
    }
    fprintf(stdout, "P6\n%d %d\n255\n", width, height);
    for (int j = 0; j < height; ++j) {
      for (int i = 0; i < width; ++i) {
        complex long double c = coordinatel(i, j, width, height, -0.75f, 1.25f);
        float hue = 7.0f * carg(n[j][i]) / (2.0f * pif);
        float sat = cabs(n[j][i]) / 2.0;
        float val = cabs(n[j][i] - c) < 4 * 1.25/height ? 0.0f : 1.0f;
        float red, grn, blu;
        hsv2rgb(hue, sat, val, &red, &grn, &blu);
        fputc(channel(red), stdout);
        fputc(channel(grn), stdout);
        fputc(channel(blu), stdout);
      }
    }
  }
  return 0;
}
