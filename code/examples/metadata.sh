#!/bin/bash
# test rays crossing image borders in both directions
# chmod +x *.sh

# angle or external parameter ray 
angle=63/33554432
# info from program Mandel by Wolf Jung http://mndynamics.com/indexp.html
# The angle  63/33554432  or  0000000000000000000111111
# has  preperiod = 25  and  period = 1.
# Entropy: e^h = 2^B = λ = 1.14436377
# The corresponding parameter ray is landing at a Misiurewicz point of preperiod 25 and period dividing 1.




# zoom exponent : if zoom = 0 then no zoom, if 1 then radius is 10 times smaller
zoom=1
# zoom=30 ./render using MPFR<114> time real	10m2.216s




# depth of drawing external ray 
depth=1000 
# inbits are proportional to zoom 
inbits=$((54+$((4*$zoom))))


# plane description : view = center and radius 
# center can be aproximated by last point of the ray 
# ./ray_in 63/33554432 1500 > ray.txt
center="2.75440192442997482763156942365466633744567027411676641366606788332e-1 6.43937272885086260909220730125585919267993168198975635957506121777e-3"
radius=" 3.0e-"$zoom
view=$center" "$radius

#
pngfilename=$zoom".png"
#
./render $view && ./colour > out.ppm && ./annotate out.ppm  $pngfilename <<EOF
rgba 1 1 1 1
line ./ray_in $angle $depth | ./rescale $inbits 53 $view 0
EOF

echo "Image " $pngfilename "is saved"



# info text file 
txtfilename=$zoom".txt"
echo "part of complex plane " > $txtfilename
echo "center of image c = " $center >> $txtfilename
echo "radius = (image height/2) = " $radius >> $txtfilename
echo "zoom level = 10^" $zoom >>$txtfilename
echo "inbits = " $inbits >> $txtfilename
echo "angle of external ray  = " $angle  "in turns ">> $txtfilename
echo "info text file " $txtfilename "is saved"





