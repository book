#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <mpfr.h>
#include "mandelbrot_nucleus.h"
#include "mandelbrot_interior.h"
#include "mpfr_complex.h"
#include "pi.h"

int main(int argc, char **argv) {
  int prec = 100;

  // 1 -> 1/3 -> 3 -> 1/89 -> 267

  // nucleus of cardioid
  mpfr_t nucleus_1_x, nucleus_1_y;
  mpfr_init2(nucleus_1_x, prec);
  mpfr_init2(nucleus_1_y, prec);
  mpfr_set_d(nucleus_1_x, 0.0, GMP_RNDN);
  mpfr_set_d(nucleus_1_y, 0.0, GMP_RNDN);
  // nucleus of 1/3 bulb
  mpfr_t nucleus_3_x, nucleus_3_y;
  mpfr_init2(nucleus_3_x, prec);
  mpfr_init2(nucleus_3_y, prec);
  mpfr_set_d(nucleus_3_x, -1.1704915364583335e-01, GMP_RNDN);
  mpfr_set_d(nucleus_3_y, 7.3591766357421873e-01, GMP_RNDN);
  mandelbrot_nucleus(nucleus_3_x, nucleus_3_y, nucleus_3_x, nucleus_3_y, 3, 64);
  // 1/89 bond
  mpfr_t x, y;
  mpfr_init2(x, prec);
  mpfr_init2(y, prec);
  complex double z = cexp(I * 2 * pi * 1.0 / 89.0);
  mpfr_set_d(x, creal(z), GMP_RNDN);
  mpfr_set_d(y, cimag(z), GMP_RNDN);
  mpfr_t bond_89_x, bond_89_y, wucleus_89_x, wucleus_89_y;
  mpfr_init2(bond_89_x, prec);
  mpfr_init2(bond_89_y, prec);
  mpfr_init2(wucleus_89_x, prec);
  mpfr_init2(wucleus_89_y, prec);
  mpfr_set(bond_89_x, nucleus_3_x, GMP_RNDN);
  mpfr_set(bond_89_y, nucleus_3_y, GMP_RNDN);
  mpfr_set(wucleus_89_x, nucleus_3_x, GMP_RNDN);
  mpfr_set(wucleus_89_y, nucleus_3_y, GMP_RNDN);
  mandelbrot_interior(wucleus_89_x, wucleus_89_y, bond_89_x, bond_89_y, wucleus_89_x, wucleus_89_y, bond_89_x, bond_89_y, 3, x, y, 64);
  mpfr_printf("text `echo '%Re %Re' | ./rescale 100 53 $view 0` 1/89\n", bond_89_x, bond_89_y);
  // bond to 267 nucleus
  mpfr_t r;
  mpfr_init2(r, prec);
  mpfr_sub(x, bond_89_x, nucleus_3_x, GMP_RNDN);
  mpfr_sub(y, bond_89_y, nucleus_3_y, GMP_RNDN);
  mpfr_t t0, t1;
  mpfr_init2(t0, prec);
  mpfr_init2(t1, prec);
  cmpfr_abs2(r, x, y, t0, t1, GMP_RNDN);
  mpfr_sqrt(r, r, GMP_RNDN);
  // use p/q child size estimate 1/q^2
  double e = 1.0/89.0;
  e *= e;
  mpfr_mul_d(r, r, e, GMP_RNDN);
  mpfr_mul(x, x, r, GMP_RNDN);
  mpfr_mul(y, y, r, GMP_RNDN);
  mpfr_t nucleus_267_x, nucleus_267_y;
  mpfr_init2(nucleus_267_x, prec);
  mpfr_init2(nucleus_267_y, prec);
  mpfr_add(nucleus_267_x, bond_89_x, x, GMP_RNDN);
  mpfr_add(nucleus_267_y, bond_89_y, y, GMP_RNDN);
  mandelbrot_nucleus(nucleus_267_x, nucleus_267_y, nucleus_267_x, nucleus_267_y, 267, 64);
  mpfr_printf("text `echo '%Re %Re' | ./rescale 100 53 $view 0` 267\n", nucleus_267_x, nucleus_267_y);

  // 1 -> 89/268 -> 268
  
  // 89/268 bond
  z = cexp(I * 2 * pi * 89.0 / 268.0);
  mpfr_set_d(x, creal(z), GMP_RNDN);
  mpfr_set_d(y, cimag(z), GMP_RNDN);
  mpfr_set(bond_89_x, nucleus_1_x, GMP_RNDN);
  mpfr_set(bond_89_y, nucleus_1_y, GMP_RNDN);
  mpfr_set(wucleus_89_x, nucleus_1_x, GMP_RNDN);
  mpfr_set(wucleus_89_y, nucleus_1_y, GMP_RNDN);
  mandelbrot_interior(wucleus_89_x, wucleus_89_y, bond_89_x, bond_89_y, wucleus_89_x, wucleus_89_y, bond_89_x, bond_89_y, 1, x, y, 64);
  mpfr_printf("text `echo '%Re %Re' | ./rescale 100 53 $view 0` 89/268\n", bond_89_x, bond_89_y);
  // bond to 268 nucleus
  mpfr_sub(x, bond_89_x, nucleus_1_x, GMP_RNDN);
  mpfr_sub(y, bond_89_y, nucleus_1_y, GMP_RNDN);
  cmpfr_abs2(r, x, y, t0, t1, GMP_RNDN);
  mpfr_sqrt(r, r, GMP_RNDN);
  // use p/q child size estimate sin(pi/q)/q^2
  e = 1.0/268.0;
  e = sin(pi * e) * e * e;
  mpfr_mul_d(r, r, e, GMP_RNDN);
  mpfr_mul(x, x, r, GMP_RNDN);
  mpfr_mul(y, y, r, GMP_RNDN);
  mpfr_add(nucleus_267_x, bond_89_x, x, GMP_RNDN);
  mpfr_add(nucleus_267_y, bond_89_y, y, GMP_RNDN);
  mandelbrot_nucleus(nucleus_267_x, nucleus_267_y, nucleus_267_x, nucleus_267_y, 268, 64);
  mpfr_printf("text `echo '%Re %Re' | ./rescale 100 53 $view 0` 268\n", nucleus_267_x, nucleus_267_y);

  // TODO cleanup
  return 0;
}
