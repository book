#!/bin/bash
# test filling
view="-0.75 0 1.5"
./render $view && ./colour > out.ppm && ./annotate out.ppm <<EOF
rgba 1 1 1 0.5
fill ./bin/mandelbrot_boundary 53  0 0 1 100 1000 | ./rescale 53 53 $view 0
fill ./bin/mandelbrot_boundary 53 -1 0 2 100 1000 | ./rescale 53 53 $view 0
EOF
