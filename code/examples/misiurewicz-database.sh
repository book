#!/bin/bash
cpucores=4
mkdir -p misiurewicz-database
cd misiurewicz-database
../bin/mandelbrot_misiurewicz_landings 2 16
ls landing_*_*.txt | xargs -n 1 -P "${cpucores}" ../bin/mandelbrot_misiurewicz_grouping
cd ..
du -hs misiurewicz-database
