#!/bin/bash
# default view with multiple rays
view="-0.75 0 1.5"
./render $view && ./colour > out.ppm && ./annotate out.ppm <<EOF
rgba 1 1 1 1
line ./ray_out 53  0.26  0    | ./rescale 53 53 $view 0
text `echo        "0.26  0"   | ./rescale 53 53 $view 0`  0.26+0.00i
line ./ray_out 53 -2.01  0    | ./rescale 53 53 $view 0
text `echo       "-2.01  0"   | ./rescale 53 53 $view 0` -2.01+0.00i
line ./ray_out 53 -0.75  0.01 | ./rescale 53 53 $view 0
text `echo       "-0.75  0.01"| ./rescale 53 53 $view 0` -0.75+0.01i
line ./ray_out 53 -1.25 -0.25 | ./rescale 53 53 $view 0
text `echo       "-1.25 -0.25"| ./rescale 53 53 $view 0` -1.25-0.25i
line ./ray_out 53 -0.3  -0.75 | ./rescale 53 53 $view 0
text `echo       "-0.3  -0.75"| ./rescale 53 53 $view 0` -0.30-0.75i
EOF
