#include <math.h>
#include <stdlib.h>
#include "mandelbrot_external_ray_in_native.h"
#include "pi.h"

static double cabs2(complex double z) {
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

struct mandelbrot_external_ray_in_native {
  mpq_t angle;
  mpq_t one;
  unsigned int sharpness; // number of steps to take within each dwell band
  double escape_radius;
  complex double c;
  unsigned int j;
  unsigned int k;
};

extern struct mandelbrot_external_ray_in_native *mandelbrot_external_ray_in_native_new(mpq_t angle) {
  struct mandelbrot_external_ray_in_native *r = malloc(sizeof(struct mandelbrot_external_ray_in_native));
  mpq_init(r->angle);
  mpq_set(r->angle, angle);
  mpq_init(r->one);
  mpq_set_ui(r->one, 1, 1);
  r->sharpness = 4;
  r->escape_radius = 65536.0;
  double a = 2.0 * pi * mpq_get_d(r->angle);
  r->c = r->escape_radius * (cos(a) * I * sin(a));
  r->k = 0;
  r->j = 0;
  return r;
}

extern void mandelbrot_external_ray_in_native_delete(struct mandelbrot_external_ray_in_native *r) {
  mpq_clear(r->angle);
  mpq_clear(r->one);
  free(r);
}

extern bool mandelbrot_external_ray_in_native_step(struct mandelbrot_external_ray_in_native *r) {
  if (r->j >= r->sharpness) {
    mpq_mul_2exp(r->angle, r->angle, 1);
    if (mpq_cmp_ui(r->angle, 1, 1) >= 0) {
      mpq_sub(r->angle, r->angle, r->one);
    }
    r->k++;
    r->j = 0;
  }
  // r0 <- er ** ((1/2) ** ((j + 0.5)/sharpness))
  // t0 <- r0 cis(2 * pi * angle)
  double r0 = pow(r->escape_radius, pow(0.5, (r->j + 0.5) / r->sharpness));
  double a0 = 2.0 * pi * mpq_get_d(r->angle);
  complex double t0 = r0 * (cos(a0) + I * sin(a0));
  // c <- r->c
  complex double cc = r->c;
  for (unsigned int i = 0; i < 64; ++i) { // FIXME arbitrary limit
    complex double z = 0;
    complex double dz = 0;
    for (unsigned int p = 0; p <= r->k; ++p) {
      dz = 2 * z * dz + 1;
      z = z * z + cc;
    }
    complex double ccc = cc - (z - t0) / dz;
    double delta2 = cabs2(ccc - cc);
    bool converged = ! (delta2 > 0);
    if (converged) {
      r->j = r->j + 1;
      r->c = cc;
      return true;
    }
    cc = ccc;
  }
  r->j = r->j + 1;
  r->c = cc;
  return false;
}

extern complex double mandelbrot_external_ray_in_native_get(struct mandelbrot_external_ray_in_native *r) {
  return r->c;
}
