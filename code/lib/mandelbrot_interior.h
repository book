#ifndef MANDELBROT_INTERIOR_H
#define MANDELBROT_INTERIOR_H 1

#include <mpfr.h>

extern int mandelbrot_interior(mpfr_t wucleusx, mpfr_t wucleusy, mpfr_t bondx, mpfr_t bondy, const mpfr_t wucleusguessx, const mpfr_t wucleusguessy, const mpfr_t bondguessx, const mpfr_t bondguessy, int period, const mpfr_t interiorx, const mpfr_t interiory, int maxiters);

#endif
