#include <complex.h>
#include <math.h>
#include "mandelbrot_render.h"
#include "mpfr_complex.h"

static void coordinate_mpfr(int i, int j, int width, int height, const mpfr_t cre, const mpfr_t cim, const mpfr_t radius, mpfr_t x, mpfr_t y) {
  mpfr_set_d(x, (i + 0.5 - width /2.0) / (height/2.0), GMP_RNDN);
  mpfr_set_d(y,-(j + 0.5 - height/2.0) / (height/2.0), GMP_RNDN);
  mpfr_mul(x, x, radius, GMP_RNDN);
  mpfr_mul(y, y, radius, GMP_RNDN);
  mpfr_add(x, x, cre, GMP_RNDN);
  mpfr_add(y, y, cim, GMP_RNDN);
}

static int wucleus_mpfr(const mpfr_t z0re, const mpfr_t z0im, const mpfr_t cre, const mpfr_t cim, const mpfr_t pixel_spacing, int period, int max_iters, mpfr_t zre_out, mpfr_t zim_out, mpfr_t dzre_out, mpfr_t dzim_out) {
  int retval = 0;
  int bits = 2 * mpfr_get_prec(cre);
#define VARS eps, zzre, zzim, zre, zim, dzre, dzim, zz1re, zz1im, dz1re, dz1im, d, t0, t1, t2, t3
  mpfr_t VARS;
  mpfr_inits2(bits, VARS, (mpfr_ptr) 0);
  mpfr_sqr(eps, pixel_spacing, GMP_RNDN);
  mpfr_div_d(eps, eps, 65536.0, GMP_RNDN);
  // zz := z0
  cmpfr_set(zzre, zzim, z0re, z0im, GMP_RNDN);
  for (int j = 0; j < max_iters; ++j) {
    // z := zz
    cmpfr_set(zre, zim, zzre, zzim, GMP_RNDN);
    // dz := 1
    cmpfr_set_si(dzre, dzim, 1, 0, GMP_RNDN);
    for (int i = 0; i < period; ++i) {
      // dz := 2 * z * dz
      cmpfr_mul(dzre, dzim, zre, zim, dzre, dzim, t0, t1, t2, t3, GMP_RNDN);
      cmpfr_mul_2si(dzre, dzim, dzre, dzim, 1, GMP_RNDN);
      // z := z^2 + c
      cmpfr_sqr(zre, zim, zre, zim, t0, t1, GMP_RNDN);
      cmpfr_add(zre, zim, zre, zim, cre, cim, GMP_RNDN);
    }
    // zz1 := zz - (z  - zz) / (dz - 1)
    cmpfr_sub(zz1re, zz1im, zre, zim, zzre, zzim, GMP_RNDN);
    cmpfr_sub_d(dz1re, dz1im, dzre, dzim, 1.0, 0.0, GMP_RNDN);
    cmpfr_div(zz1re, zz1im, zz1re, zz1im, dz1re, dz1im, t0, t1, t2, t3, GMP_RNDN);
    cmpfr_sub(zz1re, zz1im, zzre, zzim, zz1re, zz1im, GMP_RNDN);
    // d := |zz1 - zz|^2
    cmpfr_sub(t0, t1, zz1re, zz1im, zzre, zzim, GMP_RNDN);
    cmpfr_abs2(d, t0, t1, t2, t3, GMP_RNDN);
    if (mpfr_less_p(d, eps)) {
      mpfr_set_prec(zre_out, bits);
      mpfr_set_prec(zim_out, bits);
      mpfr_set_prec(dzre_out, bits);
      mpfr_set_prec(dzim_out, bits);
      cmpfr_set(zre_out, zim_out, zre, zim, GMP_RNDN);
      cmpfr_set(dzre_out, dzim_out, dzre, dzim, GMP_RNDN);
      retval = 1;
      break;
    }
    // zz := zz1
    cmpfr_set(zzre, zzim, zz1re, zz1im, GMP_RNDN);
  }
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
  return retval;
}

struct partial_mpfr {
  mpfr_t zre, zim;
  int p;
};

static void calculate_interior_mpfr(struct mandelbrot_pixel *out, const mpfr_t cre, const mpfr_t cim, int maximum_iterations, const mpfr_t escape_radius2, const mpfr_t pixel_spacing, enum mandelbrot_pixel_type bias, struct partial_mpfr *partial) {
  out->tag = mandelbrot_pixel_unescaped;
  int numpartials = 0;
#define VARS zre, zim, dcre, dcim, mz2, z2, z0re, z0im, dz0re, dz0im, z1re, z1im, dz1re, dz1im, dzdz1re, dzdz1im, dc1re, dc1im, dcdz1re, dcdz1im, t0, t1, t2, t3, t4, t5
  mpfr_t VARS;
  mpfr_inits2(mpfr_get_prec(cre), VARS, (mpfr_ptr) 0);
  // z := 0
  cmpfr_set_si(zre, zim, 0, 0, GMP_RNDN);
  // dc := 0
  cmpfr_set_si(dcre, dcim, 0, 0, GMP_RNDN);
  // mz2 := 1.0/0.0
  mpfr_set_d(mz2, 1.0 / 0.0, GMP_RNDN);
  int p = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    // dc := 2 * z * dc + 1
    cmpfr_mul(dcre, dcim, zre, zim, dcre, dcim, t0, t1, t2, t3, GMP_RNDN);
    cmpfr_mul_2si(dcre, dcim, dcre, dcim, 1, GMP_RNDN);
    mpfr_add_si(dcre, dcre, 1, GMP_RNDN);
    // z := z^2 + c
    cmpfr_sqr(zre, zim, zre, zim, t0, t1, GMP_RNDN);
    cmpfr_add(zre, zim, zre, zim, cre, cim, GMP_RNDN);
    // z2 := |z|^2
    cmpfr_abs2(z2, zre, zim, t0, t1, GMP_RNDN);
    if (mpfr_less_p(z2, mz2)) {
      // mz2 := z2;
      mpfr_set(mz2, z2, GMP_RNDN);
      p = n;
      if (bias == mandelbrot_pixel_interior) {
        // z0 := 0
        cmpfr_set_si(z0re, z0im, 0, 0, GMP_RNDN);
        cmpfr_set_si(dz0re, dz0im, 0, 0, GMP_RNDN);
        if (wucleus_mpfr(zre, zim, cre, cim, pixel_spacing, p, 64, z0re, z0im, dz0re, dz0im)) {
          // t0 := |dz0|^2
          cmpfr_abs2(t0, dz0re, dz0im, t2, t3, GMP_RNDN);
          // t1 := 1
          mpfr_set_si(t1, 1, GMP_RNDN);
          if (mpfr_lessequal_p(t0, t1)) {
            out->tag = mandelbrot_pixel_interior;
            out->u.interior.final_p = n;
            out->u.interior.final_dz = mpfr_get_d(dz0re, GMP_RNDN) + I * mpfr_get_d(dz0im, GMP_RNDN);
            // z1 := z0
            mpfr_set_prec(z1re, mpfr_get_prec(z0re));
            mpfr_set_prec(z1im, mpfr_get_prec(z0re));
            cmpfr_set(z1re, z1im, z0re, z0im, GMP_RNDN);
            // dz1 := 1
            mpfr_set_prec(dz1re, mpfr_get_prec(z0re));
            mpfr_set_prec(dz1im, mpfr_get_prec(z0re));
            cmpfr_set_si(dz1re, dz1im, 1, 0, GMP_RNDN);
            // dzdz1 := 0
            mpfr_set_prec(dzdz1re, mpfr_get_prec(z0re));
            mpfr_set_prec(dzdz1im, mpfr_get_prec(z0re));
            cmpfr_set_si(dzdz1re, dzdz1im, 0, 0, GMP_RNDN);
            // dc1 := 0
            mpfr_set_prec(dc1re, mpfr_get_prec(z0re));
            mpfr_set_prec(dc1im, mpfr_get_prec(z0re));
            cmpfr_set_si(dc1re, dc1im, 0, 0, GMP_RNDN);
            // dcdz1 := 0
            mpfr_set_prec(dcdz1re, mpfr_get_prec(z0re));
            mpfr_set_prec(dcdz1im, mpfr_get_prec(z0re));
            cmpfr_set_si(dcdz1re, dcdz1im, 0, 0, GMP_RNDN);
            mpfr_set_prec(t0, mpfr_get_prec(z0re));
            mpfr_set_prec(t1, mpfr_get_prec(z0re));
            mpfr_set_prec(t2, mpfr_get_prec(z0re));
            mpfr_set_prec(t3, mpfr_get_prec(z0re));
            mpfr_set_prec(t4, mpfr_get_prec(z0re));
            mpfr_set_prec(t5, mpfr_get_prec(z0re));
            for (int j = 0; j < p; ++j) {
              // dcdz1 := 2 * (z1 * dcdz1 + dz1 * dc1)
              cmpfr_mul(dcdz1re, dcdz1im, z1re, z1im, dcdz1re, dcdz1im, t0, t1, t2, t3, GMP_RNDN);
              cmpfr_mul(t4, t5, dz1re, dz1im, dc1re, dc1im, t0, t1, t2, t3, GMP_RNDN);
              cmpfr_add(dcdz1re, dcdz1im, dcdz1re, dcdz1im, t4, t5, GMP_RNDN);
              cmpfr_mul_2si(dcdz1re, dcdz1im, dcdz1re, dcdz1im, 1, GMP_RNDN);
              // dc1 := 2 * z1 * dc1 + 1
              cmpfr_mul(dc1re, dc1im, z1re, z1im, dc1re, dc1im, t0, t1, t2, t3, GMP_RNDN);
              cmpfr_mul_2si(dc1re, dc1im, dc1re, dc1im, 1, GMP_RNDN);
              mpfr_add_si(dc1re, dc1re, 1, GMP_RNDN);
              // dzdz1 := 2 * (z1 * dzdz1 + dz1^2)
              cmpfr_mul(dzdz1re, dzdz1im, z1re, z1im, dzdz1re, dzdz1im, t0, t1, t2, t3, GMP_RNDN);
              cmpfr_sqr(t0, t1, dz1re, dz1im, t2, t3, GMP_RNDN);
              cmpfr_add(dzdz1re, dzdz1im, dzdz1re, dzdz1im, t0, t1, GMP_RNDN);
              cmpfr_mul_2si(dzdz1re, dzdz1im, dzdz1re, dzdz1im, 1, GMP_RNDN);
              // dz1 := 2 * z1 * dz1
              cmpfr_mul(dz1re, dz1im, z1re, z1im, dz1re, dz1im, t0, t1, t2, t3, GMP_RNDN);
              cmpfr_mul_2si(dz1re, dz1im, dz1re, dz1im, 1, GMP_RNDN);
              // z1 := z1^2 + c
              cmpfr_sqr(z1re, z1im, z1re, z1im, t0, t1, GMP_RNDN);
              cmpfr_add(z1re, z1im, z1re, z1im, cre, cim, GMP_RNDN);
            }
            // de := (1 - |dz1|^2) / (|dcdz1 + dzdz11 * dc1 / (1 - dz1)| * pixel_spacing)
            cmpfr_sub_d(t4, t5, dz1re, dz1im, 1.0, 0.0, GMP_RNDN);
            cmpfr_mul(dzdz1re, dzdz1im, dzdz1re, dzdz1im, dc1re, dc1im, t0, t1, t2, t3, GMP_RNDN);
            cmpfr_div(dzdz1re, dzdz1im, dzdz1re, dzdz1im, t4, t5, t0, t1, t2, t3, GMP_RNDN);
            cmpfr_sub(dcdz1re, dcdz1im, dcdz1re, dcdz1im, dzdz1re, dzdz1im, GMP_RNDN);
            cmpfr_abs2(t5, dcdz1re, dcdz1im, t0, t1, GMP_RNDN);
            mpfr_sqrt(t5, t5, GMP_RNDN);
            mpfr_mul(t5, t5, pixel_spacing, GMP_RNDN);
            cmpfr_abs2(t4, dz1re, dz1im, t0, t1, GMP_RNDN);
            mpfr_sub_si(t4, t4, 1, GMP_RNDN);
            mpfr_div(t0, t4, t5, GMP_RNDN);
            out->u.interior.final_de = -mpfr_get_d(t0, GMP_RNDN);
            break;
          }
        }
      } else {
        mpfr_init2(partial[numpartials].zre, mpfr_get_prec(zre));
        mpfr_init2(partial[numpartials].zim, mpfr_get_prec(zim));
        mpfr_set(partial[numpartials].zre, zre, GMP_RNDN);
        mpfr_set(partial[numpartials].zim, zim, GMP_RNDN);
        partial[numpartials].p = p;
        numpartials++;
      }
    }
    if (mpfr_greater_p(z2, escape_radius2)) {
      out->tag = mandelbrot_pixel_exterior;
      out->u.exterior.final_n = n;
      out->u.exterior.final_z = mpfr_get_d(zre, GMP_RNDN) + I * mpfr_get_d(zim, GMP_RNDN);
      // de := 2 * |z| * log(|z|) / (|dc| * pixel_spacing)
      cmpfr_abs2(t4, zre, zim, t0, t1, GMP_RNDN);
      mpfr_sqrt(t4, t4, GMP_RNDN);
      mpfr_log(t5, t4, GMP_RNDN);
      cmpfr_abs2(t3, dcre, dcim, t0, t1, GMP_RNDN);
      mpfr_sqrt(t3, t3, GMP_RNDN);
      mpfr_mul(t3, t3, pixel_spacing, GMP_RNDN);
      mpfr_mul(t0, t4, t5, GMP_RNDN);
      mpfr_mul_2si(t0, t0, 1, GMP_RNDN);
      mpfr_div(t0, t0, t3, GMP_RNDN);
      out->u.exterior.final_de = mpfr_get_d(t0, GMP_RNDN);
      out->u.exterior.final_p = p;
      break;
    }
  }
  if (bias != mandelbrot_pixel_interior && out->tag == mandelbrot_pixel_unescaped) {
    for (int n = 0; n < numpartials; ++n) {
      mpfr_set(zre, partial[n].zre, GMP_RNDN);
      mpfr_set(zim, partial[n].zim, GMP_RNDN);
      p = partial[n].p;
      // z0 := 0
      cmpfr_set_si(z0re, z0im, 0, 0, GMP_RNDN);
      cmpfr_set_si(dz0re, dz0im, 0, 0, GMP_RNDN);
      if (wucleus_mpfr(zre, zim, cre, cim, pixel_spacing, p, 64, z0re, z0im, dz0re, dz0im)) {
        // t0 := |dz0|^2
        cmpfr_abs2(t0, dz0re, dz0im, t2, t3, GMP_RNDN);
        // t1 := 1
        mpfr_set_si(t1, 1, GMP_RNDN);
        if (mpfr_lessequal_p(t0, t1)) {
          out->tag = mandelbrot_pixel_interior;
          out->u.interior.final_p = p;
          out->u.interior.final_dz = mpfr_get_d(dz0re, GMP_RNDN) + I * mpfr_get_d(dz0im, GMP_RNDN);
          // z1 := z0
          mpfr_set_prec(z1re, mpfr_get_prec(z0re));
          mpfr_set_prec(z1im, mpfr_get_prec(z0re));
          cmpfr_set(z1re, z1im, z0re, z0im, GMP_RNDN);
          // dz1 := 1
          mpfr_set_prec(dz1re, mpfr_get_prec(z0re));
          mpfr_set_prec(dz1im, mpfr_get_prec(z0re));
          cmpfr_set_si(dz1re, dz1im, 1, 0, GMP_RNDN);
          // dzdz1 := 0
          mpfr_set_prec(dzdz1re, mpfr_get_prec(z0re));
          mpfr_set_prec(dzdz1im, mpfr_get_prec(z0re));
          cmpfr_set_si(dzdz1re, dzdz1im, 0, 0, GMP_RNDN);
          // dc1 := 0
          mpfr_set_prec(dc1re, mpfr_get_prec(z0re));
          mpfr_set_prec(dc1im, mpfr_get_prec(z0re));
          cmpfr_set_si(dc1re, dc1im, 0, 0, GMP_RNDN);
          // dcdz1 := 0
          mpfr_set_prec(dcdz1re, mpfr_get_prec(z0re));
          mpfr_set_prec(dcdz1im, mpfr_get_prec(z0re));
          cmpfr_set_si(dcdz1re, dcdz1im, 0, 0, GMP_RNDN);
          mpfr_set_prec(t0, mpfr_get_prec(z0re));
          mpfr_set_prec(t1, mpfr_get_prec(z0re));
          mpfr_set_prec(t2, mpfr_get_prec(z0re));
          mpfr_set_prec(t3, mpfr_get_prec(z0re));
          mpfr_set_prec(t4, mpfr_get_prec(z0re));
          mpfr_set_prec(t5, mpfr_get_prec(z0re));
          for (int j = 0; j < p; ++j) {
            // dcdz1 := 2 * (z1 * dcdz1 + dz1 * dc1)
            cmpfr_mul(dcdz1re, dcdz1im, z1re, z1im, dcdz1re, dcdz1im, t0, t1, t2, t3, GMP_RNDN);
            cmpfr_mul(t4, t5, dz1re, dz1im, dc1re, dc1im, t0, t1, t2, t3, GMP_RNDN);
            cmpfr_add(dcdz1re, dcdz1im, dcdz1re, dcdz1im, t4, t5, GMP_RNDN);
            cmpfr_mul_2si(dcdz1re, dcdz1im, dcdz1re, dcdz1im, 1, GMP_RNDN);
            // dc1 := 2 * z1 * dc1 + 1
            cmpfr_mul(dc1re, dc1im, z1re, z1im, dc1re, dc1im, t0, t1, t2, t3, GMP_RNDN);
            cmpfr_mul_2si(dc1re, dc1im, dc1re, dc1im, 1, GMP_RNDN);
            mpfr_add_si(dc1re, dc1re, 1, GMP_RNDN);
            // dzdz1 := 2 * (z1 * dzdz1 + dz1^2)
            cmpfr_mul(dzdz1re, dzdz1im, z1re, z1im, dzdz1re, dzdz1im, t0, t1, t2, t3, GMP_RNDN);
            cmpfr_sqr(t0, t1, dz1re, dz1im, t2, t3, GMP_RNDN);
            cmpfr_add(dzdz1re, dzdz1im, dzdz1re, dzdz1im, t0, t1, GMP_RNDN);
            cmpfr_mul_2si(dzdz1re, dzdz1im, dzdz1re, dzdz1im, 1, GMP_RNDN);
            // dz1 := 2 * z1 * dz1
            cmpfr_mul(dz1re, dz1im, z1re, z1im, dz1re, dz1im, t0, t1, t2, t3, GMP_RNDN);
            cmpfr_mul_2si(dz1re, dz1im, dz1re, dz1im, 1, GMP_RNDN);
            // z1 := z1^2 + c
            cmpfr_sqr(z1re, z1im, z1re, z1im, t0, t1, GMP_RNDN);
            cmpfr_add(z1re, z1im, z1re, z1im, cre, cim, GMP_RNDN);
          }
          // de := (1 - |dz1|^2) / (|dcdz1 + dzdz11 * dc1 / (1 - dz1)| * pixel_spacing)
          cmpfr_sub_d(t4, t5, dz1re, dz1im, 1.0, 0.0, GMP_RNDN);
          cmpfr_mul(dzdz1re, dzdz1im, dzdz1re, dzdz1im, dc1re, dc1im, t0, t1, t2, t3, GMP_RNDN);
          cmpfr_div(dzdz1re, dzdz1im, dzdz1re, dzdz1im, t4, t5, t0, t1, t2, t3, GMP_RNDN);
          cmpfr_sub(dcdz1re, dcdz1im, dcdz1re, dcdz1im, dzdz1re, dzdz1im, GMP_RNDN);
          cmpfr_abs2(t5, dcdz1re, dcdz1im, t0, t1, GMP_RNDN);
          mpfr_sqrt(t5, t5, GMP_RNDN);
          mpfr_mul(t5, t5, pixel_spacing, GMP_RNDN);
          cmpfr_abs2(t4, dz1re, dz1im, t0, t1, GMP_RNDN);
          mpfr_sub_si(t4, t4, 1, GMP_RNDN);
          mpfr_div(t0, t4, t5, GMP_RNDN);
          out->u.interior.final_de = -mpfr_get_d(t0, GMP_RNDN);
          break;
        }
      }
    }
  }
  if (partial) {
    for (int n = 0; n < numpartials; ++n) {
      mpfr_clear(partial[n].zre);
      mpfr_clear(partial[n].zim);
    }
  }
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
}

static void calculate_exterior_mpfr(struct mandelbrot_pixel *out, const mpfr_t cre, const mpfr_t cim, int maximum_iterations, const mpfr_t escape_radius2, const mpfr_t pixel_spacing) {
  out->tag = mandelbrot_pixel_unescaped;
#define VARS zre, zim, dcre, dcim, mz2, z2, t0, t1, t2, t3, t4, t5
  mpfr_t VARS;
  mpfr_inits2(mpfr_get_prec(cre), VARS, (mpfr_ptr) 0);
  // z := 0
  cmpfr_set_si(zre, zim, 0, 0, GMP_RNDN);
  // dc := 0
  cmpfr_set_si(dcre, dcim, 0, 0, GMP_RNDN);
  // mz2 := 1.0/0.0
  mpfr_set_d(mz2, 1.0 / 0.0, GMP_RNDN);
  int p = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    // dc := 2 * z * dc + 1
    cmpfr_mul(dcre, dcim, zre, zim, dcre, dcim, t0, t1, t2, t3, GMP_RNDN);
    cmpfr_mul_2si(dcre, dcim, dcre, dcim, 1, GMP_RNDN);
    mpfr_add_si(dcre, dcre, 1, GMP_RNDN);
    // z := z^2 + c
    cmpfr_sqr(zre, zim, zre, zim, t0, t1, GMP_RNDN);
    cmpfr_add(zre, zim, zre, zim, cre, cim, GMP_RNDN);
    // z2 := |z|^2
    cmpfr_abs2(z2, zre, zim, t0, t1, GMP_RNDN);
    if (mpfr_less_p(z2, mz2)) {
      // mz2 := z2;
      mpfr_set(mz2, z2, GMP_RNDN);
      p = n;
    }
    if (mpfr_greater_p(z2, escape_radius2)) {
      out->tag = mandelbrot_pixel_exterior;
      out->u.exterior.final_n = n;
      out->u.exterior.final_z = mpfr_get_d(zre, GMP_RNDN) + I * mpfr_get_d(zim, GMP_RNDN);
      // de := 2 * |z| * log(|z|) / (|dc| * pixel_spacing)
      cmpfr_abs2(t4, zre, zim, t0, t1, GMP_RNDN);
      mpfr_sqrt(t4, t4, GMP_RNDN);
      mpfr_log(t5, t4, GMP_RNDN);
      cmpfr_abs2(t3, dcre, dcim, t0, t1, GMP_RNDN);
      mpfr_sqrt(t3, t3, GMP_RNDN);
      mpfr_mul(t3, t3, pixel_spacing, GMP_RNDN);
      mpfr_mul(t0, t4, t5, GMP_RNDN);
      mpfr_mul_2si(t0, t0, 1, GMP_RNDN);
      mpfr_div(t0, t0, t3, GMP_RNDN);
      out->u.exterior.final_de = mpfr_get_d(t0, GMP_RNDN);
      out->u.exterior.final_p = p;
      break;
    }
  }
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
}

extern void mandelbrot_render_mpfr(struct mandelbrot_image *img, const mpfr_t cre, const mpfr_t cim, const mpfr_t radius, int maximum_iterations, const mpfr_t escape_radius2, const mpfr_t pixel_spacing, int interior, void *userdata, mandelbrot_progress_callback_t progresscb) {
  int progress = 0;
  int running = 1;
  #pragma omp parallel for schedule(dynamic, 1)
  for (int j = 0; j < img->height; ++j) {
    if (running) {
      mpfr_t x, y;
      mpfr_init2(x, mpfr_get_prec(cre));
      mpfr_init2(y, mpfr_get_prec(cre));
      struct partial_mpfr *partial = 0;
      if (interior) {
        partial = malloc(maximum_iterations * sizeof(struct partial_mpfr));
      }
      enum mandelbrot_pixel_type bias = mandelbrot_pixel_exterior;
      for (int i = 0; running && i < img->width; ++i) {
        if (running) {
          coordinate_mpfr(i, j, img->width, img->height, cre, cim, radius, x, y);
          if (interior) {
            calculate_interior_mpfr(&img->pixels[j * img->width + i], x, y, maximum_iterations, escape_radius2, pixel_spacing, bias, partial);
            bias = img->pixels[j * img->width + i].tag;
          } else {
            calculate_exterior_mpfr(&img->pixels[j * img->width + i], x, y, maximum_iterations, escape_radius2, pixel_spacing);
          }
        }
      }
      mpfr_clear(x);
      mpfr_clear(y);
      #pragma omp critical
      {
        progress++;
        if (progresscb) { running = running && progresscb(userdata, progress / (double) img->height); }
      }
      if (partial) {
        free(partial);
      }
    }
  }
}
