#ifndef MANDELBROT_RENDER_FLOAT_H
#define MANDELBROT_RENDER_FLOAT_H 1

#include "mandelbrot_image.h"

#define FTYPE float
#define FNAME(name) name ## f
#include "mandelbrot_render_native.h"
#undef FTYPE
#undef FNAME

#endif
