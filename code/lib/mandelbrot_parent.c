#include <complex.h>
#include "mandelbrot_parent.h"
#include "mandelbrot_shape.h"
#include "mandelbrot_interior.h"
#include "mandelbrot_nucleus.h"
#include "mandelbrot_wucleus.h"
#include "mpfr_complex.h"
#include "pi.h"

extern int mandelbrot_parent(mpq_t angle, mpfr_t rootx, mpfr_t rooty, mpfr_t parentx, mpfr_t parenty, const mpfr_t nucleusx, const mpfr_t nucleusy, int period, int steps, const mpfr_t epsilon, int maxiters) {
  int retval = -1;
#define VARS wux, wuy, interiorx, interiory, z0x, z0y, dz0x, dz0y, zx, zy, cx, cy, bondx, bondy, root0x, root0y, root1x, root1y, phase, pi2, mz2, t0, t1, z2, r2, one
  mpfr_t VARS;
  mpfr_inits2(mpfr_get_prec(nucleusx), VARS, (mpfr_ptr) 0);
  mpfr_set_si(one, 1, GMP_RNDN);
  switch (mandelbrot_shape(nucleusx, nucleusy, period)) {
    case mandelbrot_shape_cardioid: {
      mpfr_set_si(interiorx, 1, GMP_RNDN);
      mpfr_set_si(interiory, 0, GMP_RNDN);
      mandelbrot_interior(wux, wuy, rootx, rooty, nucleusx, nucleusy, nucleusx, nucleusy, period, interiorx, interiory, maxiters);
      mpq_set_si(angle, 0, 1);
      retval = 0;
      break;
    }
    case mandelbrot_shape_circle: {
      mpfr_t wux, wuy, interiorx, interiory;
      mpfr_init2(wux, mpfr_get_prec(nucleusx));
      mpfr_init2(wuy, mpfr_get_prec(nucleusy));
      mpfr_init2(bondx, mpfr_get_prec(nucleusx));
      mpfr_init2(bondy, mpfr_get_prec(nucleusy));
      mpfr_init2(interiorx, mpfr_get_prec(nucleusx));
      mpfr_init2(interiory, mpfr_get_prec(nucleusy));
      mpfr_set(wux, nucleusx, GMP_RNDN);
      mpfr_set(wuy, nucleusy, GMP_RNDN);
      mpfr_set(bondx, nucleusx, GMP_RNDN);
      mpfr_set(bondy, nucleusy, GMP_RNDN);
      for (int step = 0; step < steps - 1; ++step) {
        mpfr_set_d(interiorx, (step + 0.5) / steps, GMP_RNDN);
        mpfr_set_d(interiory, 0.0, GMP_RNDN);
        mandelbrot_interior(wux, wuy, bondx, bondy, wux, wuy, bondx, bondy, period, interiorx, interiory, maxiters);
      }
      mpfr_set(root0x, bondx, GMP_RNDN);
      mpfr_set(root0y, bondy, GMP_RNDN);
      mpfr_set_d(interiorx, (steps - 0.5) / steps, GMP_RNDN);
      mpfr_set_d(interiory, 0.0, GMP_RNDN);
      mandelbrot_interior(wux, wuy, bondx, bondy, wux, wuy, bondx, bondy, period, interiorx, interiory, maxiters);
      mpfr_set(root1x, bondx, GMP_RNDN);
      mpfr_set(root1y, bondy, GMP_RNDN);
      // parent := root1 + (root1 - root0) = 2 * root1 - root0
      mpfr_mul_2si(parentx, root1x, 1, GMP_RNDN);
      mpfr_mul_2si(parenty, root1y, 1, GMP_RNDN);
      mpfr_sub(parentx, parentx, root0x, GMP_RNDN);
      mpfr_sub(parenty, parenty, root0y, GMP_RNDN);
      // find interior coordinate of parent
      mpfr_set(cx, parentx, GMP_RNDN);
      mpfr_set(cy, parenty, GMP_RNDN);
      mpfr_set_si(zx, 0, GMP_RNDN);
      mpfr_set_si(zy, 0, GMP_RNDN);
      mpfr_set_d(mz2, 1.0 / 0.0, GMP_RNDN);
      for (int p = 1; p < period; ++p) {
        // z := z^2 + c
        cmpfr_sqr(zx, zy, zx, zy, t0, t1, GMP_RNDN);
        cmpfr_add(zx, zy, zx, zy, cx, cy, GMP_RNDN);
        // z2 := |z|^2
        cmpfr_abs2(z2, zx, zy, t0, t1, GMP_RNDN);
        if (mpfr_less_p(z2, mz2)) {
          mpfr_set(mz2, z2, GMP_RNDN);
          if (period % p == 0) {
            mandelbrot_wucleus(z0x, z0y, dz0x, dz0y, zx, zy, cx, cy, p, epsilon, maxiters);
            cmpfr_abs2(r2, dz0x, dz0y, t0, t1, GMP_RNDN);
            if (mpfr_less_p(r2, one)) {
              // interior to component of period p
              int denominator = period / p;
              mpfr_atan2(phase, dz0y, dz0x, GMP_RNDN);
              mpfr_const_pi(pi2, GMP_RNDN);
              mpfr_mul_2si(pi2, pi2, 1, GMP_RNDN);
              mpfr_div(phase, phase, pi2, GMP_RNDN);
              mpfr_mul_si(phase, phase, denominator, GMP_RNDN);
              int numerator = mpfr_get_si(phase, GMP_RNDN);
              if (numerator < 0) {
                numerator += denominator;
              }
              mpq_set_si(angle, numerator, denominator);
              mpq_canonicalize(angle);
              // find parent nucleus
              mandelbrot_nucleus(parentx, parenty, cx, cy, p, maxiters);
              // find bond from parent
              complex double interior = cexp(I * 2.0 * pi * numerator / (double) denominator);
              mpfr_set_d(interiorx, creal(interior), GMP_RNDN);
              mpfr_set_d(interiory, cimag(interior), GMP_RNDN);
              mandelbrot_interior(wux, wuy, rootx, rooty, parentx, parenty, parentx, parenty, p, interiorx, interiory, maxiters);
              retval = p;
              break;
            }
          }
        }
      }
      break;
    }
  }
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
  return retval;
}
