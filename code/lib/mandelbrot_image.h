#ifndef IMAGE_H
#define IMAGE_H 1

#include <complex.h>

struct mandelbrot_pixel_exterior {
  int final_n;
  int final_p;
  complex float final_z;
  float final_de;
};

struct mandelbrot_pixel_interior {
  int final_p;
  complex float final_dz;
  float final_de;
};

enum mandelbrot_pixel_type {
  mandelbrot_pixel_interior = -1,
  mandelbrot_pixel_unescaped = 0,
  mandelbrot_pixel_exterior = 1
};

struct mandelbrot_pixel {
  enum mandelbrot_pixel_type tag;
  union {
    struct mandelbrot_pixel_exterior exterior;
    struct mandelbrot_pixel_interior interior;
  } u;
};

struct mandelbrot_image {
  int width;
  int height;
  struct mandelbrot_pixel *pixels;
};

extern struct mandelbrot_image *mandelbrot_image_new(int width, int height);
extern void mandelbrot_image_free(struct mandelbrot_image *img);
extern int mandelbrot_image_save(struct mandelbrot_image *img, char *filestem, float escape_radius);
extern struct mandelbrot_pixel *mandelbrot_image_peek(struct mandelbrot_image *img, int i, int j);
extern int mandelbrot_image_in_bounds(struct mandelbrot_image *img, int i, int j);

#endif
