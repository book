#ifndef MPFR_COMPLEX_H
#define MPFR_COMPLEX_H 1

#include <mpfr.h>

extern void cmpfr_set(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, mpfr_rnd_t rnd);
extern void cmpfr_set_si(mpfr_t r_re, mpfr_t r_im, int x_re, int x_im, mpfr_rnd_t rnd);
extern void cmpfr_mul(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, const mpfr_t y_re, const mpfr_t y_im, mpfr_t t0, mpfr_t t1, mpfr_t t2, mpfr_t t3, mpfr_rnd_t rnd);
extern void cmpfr_mul_2si(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, int y, mpfr_rnd_t rnd);
extern void cmpfr_sqr(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, mpfr_t t0, mpfr_t t1, mpfr_rnd_t rnd);
extern void cmpfr_add(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, const mpfr_t y_re, const mpfr_t y_im, mpfr_rnd_t rnd);
extern void cmpfr_sub(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, const mpfr_t y_re, const mpfr_t y_im, mpfr_rnd_t rnd);
extern void cmpfr_sub_d(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, double y_re, double y_im, mpfr_rnd_t rnd);
extern void cmpfr_div(mpfr_t r_re, mpfr_t r_im, const mpfr_t x_re, const mpfr_t x_im, const mpfr_t y_re, const mpfr_t y_im, mpfr_t t0, mpfr_t t1, mpfr_t t2, mpfr_t t3, mpfr_rnd_t rnd);
extern void cmpfr_abs2(mpfr_t r, const mpfr_t x_re, const mpfr_t x_im, mpfr_t t0, mpfr_t t1, mpfr_rnd_t rnd);

#endif
