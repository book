#include <math.h>
#include "mandelbrot_render.h"
#include "mandelbrot_image.h"

static FTYPE FNAME(cabs2)(complex FTYPE z) {
  return FNAME(creal)(z) * FNAME(creal)(z) + FNAME(cimag)(z) * FNAME(cimag)(z);
}

static complex FTYPE FNAME(coordinate)(int i, int j, int width, int height, complex FTYPE center, FTYPE radius) {
  FTYPE x = (i + FNAME(0.5) - width /FNAME(2.0)) / (height/FNAME(2.0));
  FTYPE y = (j + FNAME(0.5)- height/FNAME(2.0)) / (height/FNAME(2.0));
  complex FTYPE c = center + radius * (x - I * y);
  return c;
}

static int FNAME(wucleus)(complex FTYPE *z_out, complex FTYPE *dz_out, complex FTYPE z0, complex FTYPE c, int period, int max_iters, FTYPE pixel_spacing) {
  FTYPE eps_2 = pixel_spacing * pixel_spacing;
  complex FTYPE zz = z0;
  for (int j = 0; j < max_iters; ++j) {
    complex FTYPE z = zz;
    complex FTYPE dz = 1;
    for (int i = 0; i < period; ++i) {
      dz = 2.0 * z * dz;
      z = z * z + c;
    }
    complex FTYPE zz1 = zz - (z  - zz) / (dz - FNAME(1.0));
    if (FNAME(cabs2)(zz1 - zz) < eps_2) {
      *z_out = z;
      *dz_out = dz;
      return 1;
    }
    zz = zz1;
  }
  return 0;
}

struct FNAME(partial) {
  int p;
  complex FTYPE z;
};

static void FNAME(calculate_interior)(struct mandelbrot_pixel *out, complex FTYPE c, int maximum_iterations, FTYPE escape_radius2, FTYPE pixel_spacing, enum mandelbrot_pixel_type bias, struct FNAME(partial) *partial) {
  out->tag = mandelbrot_pixel_unescaped;
  int numpartials = 0;
  complex FTYPE z = 0;
  complex FTYPE dc = 0;
  FTYPE mz2 = FNAME(1.0) / FNAME(0.0);
  int p = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    dc = 2 * z * dc + 1;
    z = z * z + c;
    FTYPE z2 = FNAME(cabs2)(z);
    if (z2 < mz2) {
      mz2 = z2;
      p = n;
      if (bias == mandelbrot_pixel_interior) {
        complex FTYPE z0 = 0.0;
        complex FTYPE dz0 = 0.0;
        if (FNAME(wucleus)(&z0, &dz0, z, c, p, 64, pixel_spacing / FNAME(4.0))) {
          if (FNAME(cabs2)(dz0) <= FNAME(1.0)) {
            out->tag = mandelbrot_pixel_interior;
            out->u.interior.final_p = p;
            out->u.interior.final_dz = dz0;
            complex FTYPE z1 = z0;
            complex FTYPE dz1 = 1.0;
            complex FTYPE dzdz1 = 0.0;
            complex FTYPE dc1 = 0.0;
            complex FTYPE dcdz1 = 0.0;
            for (int j = 0; j < p; ++j) {
              dcdz1 = 2.0 * (z1 * dcdz1 + dz1 * dc1);
              dc1 = 2.0 * z1 * dc1 + 1.0;
              dzdz1 = 2.0 * (dz1 * dz1 + z1 * dzdz1);
              dz1 = 2.0 * z1 * dz1;
              z1 = z1 * z1 + c;
            }
            out->u.interior.final_de = (FNAME(1.0) - FNAME(cabs2)(dz1)) / (FNAME(cabs)(dcdz1 + dzdz1 * dc1 / (FNAME(1.0) - dz1)) * pixel_spacing);
            break;
          }
        }
      } else {
        partial[numpartials].p = p;
        partial[numpartials].z = z;
        numpartials++;
      }
    }
    if (z2 > escape_radius2) {
      out->tag = mandelbrot_pixel_exterior;
      out->u.exterior.final_n = n;
      out->u.exterior.final_z = z;
      out->u.exterior.final_de = 2 * FNAME(cabs)(z) * FNAME(log)(FNAME(cabs)(z)) / (FNAME(cabs)(dc) * pixel_spacing);
      out->u.exterior.final_p = p;
      break;
    }
  }
  if (bias != mandelbrot_pixel_interior && out->tag == mandelbrot_pixel_unescaped) {
    for (int n = 0; n < numpartials; ++n) {
      p = partial[n].p;
      z = partial[n].z;
      complex FTYPE z0 = 0.0;
      complex FTYPE dz0 = 0.0;
      if (FNAME(wucleus)(&z0, &dz0, z, c, p, 64, pixel_spacing / FNAME(4.0))) {
        if (FNAME(cabs2)(dz0) <= FNAME(1.0)) {
          out->tag = mandelbrot_pixel_interior;
          out->u.interior.final_p = p;
          out->u.interior.final_dz = dz0;
          complex FTYPE z1 = z0;
          complex FTYPE dz1 = 1.0;
          complex FTYPE dzdz1 = 0.0;
          complex FTYPE dc1 = 0.0;
          complex FTYPE dcdz1 = 0.0;
          for (int j = 0; j < p; ++j) {
            dcdz1 = 2.0 * (z1 * dcdz1 + dz1 * dc1);
            dc1 = 2.0 * z1 * dc1 + 1.0;
            dzdz1 = 2.0 * (dz1 * dz1 + z1 * dzdz1);
            dz1 = 2.0 * z1 * dz1;
            z1 = z1 * z1 + c;
          }
          out->u.interior.final_de = (FNAME(1.0) - FNAME(cabs2)(dz1)) / (FNAME(cabs)(dcdz1 + dzdz1 * dc1 / (FNAME(1.0) - dz1)) * pixel_spacing);
          break;
        }
      }
    }
  }
}

static void FNAME(calculate_exterior)(struct mandelbrot_pixel *out, complex FTYPE c, int maximum_iterations, FTYPE escape_radius2, FTYPE pixel_spacing) {
  out->tag = mandelbrot_pixel_unescaped;
  complex FTYPE z = 0;
  complex FTYPE dc = 0;
  FTYPE mz2 = FNAME(1.0) / FNAME(0.0);
  int p = 0;
  for (int n = 1; n < maximum_iterations; ++n) {
    dc = 2 * z * dc + 1;
    z = z * z + c;
    FTYPE z2 = FNAME(cabs2)(z);
    if (z2 < mz2) {
      mz2 = z2;
      p = n;
    }
    if (z2 > escape_radius2) {
      out->tag = mandelbrot_pixel_exterior;
      out->u.exterior.final_n = n;
      out->u.exterior.final_z = z;
      out->u.exterior.final_de = 2 * FNAME(cabs)(z) * FNAME(log)(FNAME(cabs)(z)) / (FNAME(cabs)(dc) * pixel_spacing);
      out->u.exterior.final_p = p;
      break;
    }
  }
}

extern void FNAME(mandelbrot_render)(struct mandelbrot_image *img, complex FTYPE center, FTYPE radius, int maximum_iterations, FTYPE escape_radius2, FTYPE pixel_spacing, int interior, void *userdata, mandelbrot_progress_callback_t progresscb) {
  int progress = 0;
  int running = 1;
  #pragma omp parallel for schedule(dynamic, 1)
  for (int j = 0; j < img->height; ++j) {
    if (running) {
      enum mandelbrot_pixel_type bias = mandelbrot_pixel_exterior;
      struct FNAME(partial) *partial = 0;
      if (interior) {
        partial = malloc(maximum_iterations * sizeof(struct FNAME(partial)));
      }
      for (int i = 0; i < img->width; ++i) {
        if (running) {
          complex FTYPE c = FNAME(coordinate)(i, j, img->width, img->height, center, radius);
          if (interior) {
            FNAME(calculate_interior)(&img->pixels[j * img->width + i], c, maximum_iterations, escape_radius2, pixel_spacing, bias, partial);
            bias = img->pixels[j * img->width + i].tag;
          } else {
            FNAME(calculate_exterior)(&img->pixels[j * img->width + i], c, maximum_iterations, escape_radius2, pixel_spacing);
          }
        }
      }
      #pragma omp critical
      {
        progress++;
        if (progresscb) { running = running && progresscb(userdata, progress / (double) img->height); }
      }
      if (partial) {
        free(partial);
      }
    }
  }
}
