#include <complex.h>
#include <math.h>
#include <stdlib.h>
#include "mandelbrot_external_ray_out.h"
#include "mpfr_complex.h"
#include "pi.h"

static double dwell(double log_escape_radius_2, int n, double zmag2) {
  return n - log2(log(zmag2) / log_escape_radius_2);
}

struct mandelbrot_external_ray_out {
  int bits;
  int sharpness;
  int accuracy;
  double escape_radius;
  double escape_radius_2;
  double log_escape_radius_2;
  mpfr_t eps_2, cre, cim, zre, zim, zre2, zim2, zmag2, cccre, cccim,
    ccre, ccim, tmp0, tmp1, tmp2, tmp3, wre, wim, dcre, dcim,
    deltare, deltaim, ccc1re, ccc1im, ccc2re, ccc2im, cc1re, cc1im,
    cc2re, cc2im, w1re, w1im, dc1re, dc1im, w2re, w2im, dc2re, dc2im,
    delta1re, delta1im, delta2re, delta2im, cc0re, cc0im, oldcre, oldcim;
  int n;
  double d;
  int oldn;
  int bit_to_push;
};

extern struct mandelbrot_external_ray_out *mandelbrot_external_ray_out_new(const mpfr_t c0re, const mpfr_t c0im) {
  struct mandelbrot_external_ray_out *r = malloc(sizeof(struct mandelbrot_external_ray_out));
  r->bits = mpfr_get_prec(c0re);
  r->sharpness = 16;
  r->accuracy = 16;
  r->escape_radius = 1024.0;
  r->escape_radius_2 = r->escape_radius * r->escape_radius;
  r->log_escape_radius_2 = log(r->escape_radius_2);
  mpfr_init2(r->eps_2, r->bits);
  mpfr_init2(r->cre, r->bits);
  mpfr_init2(r->cim, r->bits);
  mpfr_init2(r->zre, r->bits);
  mpfr_init2(r->zim, r->bits);
  mpfr_init2(r->zre2, r->bits);
  mpfr_init2(r->zim2, r->bits);
  mpfr_init2(r->zmag2, r->bits);
  mpfr_init2(r->cccre, r->bits);
  mpfr_init2(r->cccim, r->bits);
  mpfr_init2(r->ccre, r->bits);
  mpfr_init2(r->ccim, r->bits);
  mpfr_init2(r->tmp0, r->bits);
  mpfr_init2(r->tmp1, r->bits);
  mpfr_init2(r->tmp2, r->bits);
  mpfr_init2(r->tmp3, r->bits);
  mpfr_init2(r->wre, r->bits);
  mpfr_init2(r->wim, r->bits);
  mpfr_init2(r->dcre, r->bits);
  mpfr_init2(r->dcim, r->bits);
  mpfr_init2(r->deltare, r->bits);
  mpfr_init2(r->deltaim, r->bits);
  mpfr_init2(r->ccc1re, r->bits);
  mpfr_init2(r->ccc1im, r->bits);
  mpfr_init2(r->ccc2re, r->bits);
  mpfr_init2(r->ccc2im, r->bits);
  mpfr_init2(r->cc1re, r->bits);
  mpfr_init2(r->cc1im, r->bits);
  mpfr_init2(r->cc2re, r->bits);
  mpfr_init2(r->cc2im, r->bits);
  mpfr_init2(r->w1re, r->bits);
  mpfr_init2(r->w1im, r->bits);
  mpfr_init2(r->dc1re, r->bits);
  mpfr_init2(r->dc1im, r->bits);
  mpfr_init2(r->w2re, r->bits);
  mpfr_init2(r->w2im, r->bits);
  mpfr_init2(r->dc2re, r->bits);
  mpfr_init2(r->dc2im, r->bits);
  mpfr_init2(r->delta1re, r->bits);
  mpfr_init2(r->delta1im, r->bits);
  mpfr_init2(r->delta2re, r->bits);
  mpfr_init2(r->delta2im, r->bits);
  mpfr_init2(r->cc0re, r->bits);
  mpfr_init2(r->cc0im, r->bits);
  mpfr_init2(r->oldcre, r->bits);
  mpfr_init2(r->oldcim, r->bits);
  // c := c0
  mpfr_set(r->cre, c0re, GMP_RNDN);
  mpfr_set(r->cim, c0im, GMP_RNDN);
  // eps_2 := 2 ^ (2 * (4 - bits))
  mpfr_set_si(r->eps_2, 1, GMP_RNDN);
  mpfr_mul_2si(r->eps_2, r->eps_2, 2 * (r->accuracy - r->bits), GMP_RNDN);
  // n, z, d := iterate z -> z^2 + c with z0 = 0
  r->n = 0;
  mpfr_set_si(r->zre, 0, GMP_RNDN);
  mpfr_set_si(r->zim, 0, GMP_RNDN);
  mpfr_set_si(r->zre2, 0, GMP_RNDN);
  mpfr_set_si(r->zim2, 0, GMP_RNDN);
  mpfr_set_si(r->zmag2, 0, GMP_RNDN);
  while (mpfr_get_d(r->zmag2, GMP_RNDN) < r->escape_radius_2) {
    r->n = r->n + 1;
    mpfr_mul(r->zim, r->zre, r->zim, GMP_RNDN);
    mpfr_mul_2si(r->zim, r->zim, 1, GMP_RNDN);
    mpfr_sub(r->zre, r->zre2, r->zim2, GMP_RNDN);
    mpfr_add(r->zre, r->zre, r->cre, GMP_RNDN);
    mpfr_add(r->zim, r->zim, r->cim, GMP_RNDN);
    mpfr_sqr(r->zre2, r->zre, GMP_RNDN);
    mpfr_sqr(r->zim2, r->zim, GMP_RNDN);
    mpfr_add(r->zmag2, r->zre2, r->zim2, GMP_RNDN);
  }
  r->d = dwell(r->log_escape_radius_2, r->n, mpfr_get_d(r->zmag2, GMP_RNDN));
  return r;
}

extern int mandelbrot_external_ray_out_step(struct mandelbrot_external_ray_out *r) {
  r->d -= 1.0 / r->sharpness;
  if (r->d <= 0.0) {
    return 0;
  }
  int m = ceil(r->d);
  double rr = pow(r->escape_radius, pow(2.0, m - r->d));
  double a = carg(mpfr_get_d(r->zre, GMP_RNDN) + I * mpfr_get_d(r->zim, GMP_RNDN)) / (2 * pi);
  double t = a - floor(a);
  complex double k0 = rr * cexp(I * 2 * pi *  t);
  complex double k1 = rr * cexp(I *     pi *  t);
  complex double k2 = rr * cexp(I *     pi * (t + 1));
  // oldc := c
  cmpfr_set(r->oldcre, r->oldcim, r->cre, r->cim, GMP_RNDN);
  // ccc := c
  cmpfr_set(r->cccre, r->cccim, r->cre, r->cim, GMP_RNDN);
  int limit = 64;
  do {
    // cc := ccc
    cmpfr_set(r->ccre, r->ccim, r->cccre, r->cccim, GMP_RNDN);
    // z0 := iterate w->w^2 + cc, dc -> 2 * w * dc + 1 with z0 = 0 for m iterations
    cmpfr_set_si(r->wre, r->wim, 0, 0, GMP_RNDN);
    cmpfr_set_si(r->dcre, r->dcim, 0, 0, GMP_RNDN);
    for (int i = 0; i < m; ++i) {
      // dc := 2 * w * dc + 1
      cmpfr_mul(r->dcre, r->dcim, r->wre, r->wim, r->dcre, r->dcim, r->tmp0, r->tmp1, r->tmp2, r->tmp3, GMP_RNDN);
      cmpfr_mul_2si(r->dcre, r->dcim, r->dcre, r->dcim, 1, GMP_RNDN);
      mpfr_add_si(r->dcre, r->dcre, 1, GMP_RNDN);
      // w := w^2 + cc
      cmpfr_sqr(r->wre, r->wim, r->wre, r->wim, r->tmp0, r->tmp1, GMP_RNDN);
      cmpfr_add(r->wre, r->wim, r->wre, r->wim, r->ccre, r->ccim, GMP_RNDN);
    }
    // delta := (w - k0) / dc
    cmpfr_sub_d(r->wre, r->wim, r->wre, r->wim, creal(k0), cimag(k0), GMP_RNDN);
    cmpfr_div(r->deltare, r->deltaim, r->wre, r->wim, r->dcre, r->dcim, r->tmp0, r->tmp1, r->tmp2, r->tmp3, GMP_RNDN);
    // ccc := cc - delta
    cmpfr_sub(r->cccre, r->cccim, r->ccre, r->ccim, r->deltare, r->deltaim, GMP_RNDN);
    // delta := cc - ccc
    cmpfr_sub(r->deltare, r->deltaim, r->cccre, r->cccim, r->ccre, r->ccim, GMP_RNDN);
    cmpfr_abs2(r->deltare, r->deltare, r->deltaim, r->tmp0, r->tmp1, GMP_RNDN);
  } while (mpfr_greater_p(r->deltare, r->eps_2) && --limit);
  cmpfr_set(r->cc0re, r->cc0im, r->ccre, r->ccim, GMP_RNDN);
  if (m != r->n) {
    // ccc := c
    cmpfr_set(r->ccc1re, r->ccc1im, r->cre, r->cim, GMP_RNDN);
    cmpfr_set(r->ccc2re, r->ccc2im, r->cre, r->cim, GMP_RNDN);
    limit = 64;
    do {
      // cc := ccc
      cmpfr_set(r->cc1re, r->cc1im, r->ccc1re, r->ccc1im, GMP_RNDN);
      cmpfr_set(r->cc2re, r->cc2im, r->ccc2re, r->ccc2im, GMP_RNDN);
      // w, dc := 0
      cmpfr_set_si(r->w1re, r->w1im, 0, 0, GMP_RNDN);
      cmpfr_set_si(r->w2re, r->w2im, 0, 0, GMP_RNDN);
      cmpfr_set_si(r->dc1re, r->dc1im, 0, 0, GMP_RNDN);
      cmpfr_set_si(r->dc2re, r->dc2im, 0, 0, GMP_RNDN);
      // w := iterate w->w^2 + cc, dc -> 2 * w * dc + 1 with w = 0 for m iterations
      for (int i = 0; i < m; ++i) {
        // dc1 := 2 * w1 * dc1 + 1
        cmpfr_mul(r->dc1re, r->dc1im, r->w1re, r->w1im, r->dc1re, r->dc1im, r->tmp0, r->tmp1, r->tmp2, r->tmp3, GMP_RNDN);
        cmpfr_mul_2si(r->dc1re, r->dc1im, r->dc1re, r->dc1im, 1, GMP_RNDN);
        mpfr_add_si(r->dc1re, r->dc1re, 1, GMP_RNDN);
        // w1 := w1^2 + cc1
        cmpfr_sqr(r->w1re, r->w1im, r->w1re, r->w1im, r->tmp0, r->tmp1, GMP_RNDN);
        cmpfr_add(r->w1re, r->w1im, r->w1re, r->w1im, r->cc1re, r->cc1im, GMP_RNDN);
        // dc2 := 2 * w2 * dc2 + 1
        cmpfr_mul(r->dc2re, r->dc2im, r->w2re, r->w2im, r->dc2re, r->dc2im, r->tmp0, r->tmp1, r->tmp2, r->tmp3, GMP_RNDN);
        cmpfr_mul_2si(r->dc2re, r->dc2im, r->dc2re, r->dc2im, 1, GMP_RNDN);
        mpfr_add_si(r->dc2re, r->dc2re, 1, GMP_RNDN);
        // w2 := w2^2 + cc2
        cmpfr_sqr(r->w2re, r->w2im, r->w2re, r->w2im, r->tmp0, r->tmp1, GMP_RNDN);
        cmpfr_add(r->w2re, r->w2im, r->w2re, r->w2im, r->cc2re, r->cc2im, GMP_RNDN);
      }
      // delta1 := (w1 - k1) / dc1
      cmpfr_sub_d(r->w1re, r->w1im, r->w1re, r->w1im, creal(k1), cimag(k1), GMP_RNDN);
      cmpfr_div(r->delta1re, r->delta1im, r->w1re, r->w1im, r->dc1re, r->dc1im, r->tmp0, r->tmp1, r->tmp2, r->tmp3, GMP_RNDN);
      // delta2 := (w2 - k2) / dc2
      cmpfr_sub_d(r->w2re, r->w2im, r->w2re, r->w2im, creal(k2), cimag(k2), GMP_RNDN);
      cmpfr_div(r->delta2re, r->delta2im, r->w2re, r->w2im, r->dc2re, r->dc2im, r->tmp0, r->tmp1, r->tmp2, r->tmp3, GMP_RNDN);
      // ccc1 := cc1 - delta1
      cmpfr_sub(r->ccc1re, r->ccc1im, r->cc1re, r->cc1im, r->delta1re, r->delta1im, GMP_RNDN);
      // ccc2 := cc2 - delta2
      cmpfr_sub(r->ccc2re, r->ccc2im, r->cc2re, r->cc2im, r->delta2re, r->delta2im, GMP_RNDN);
      // delta1 := cc1 - ccc1
      cmpfr_sub(r->delta1re, r->delta1im, r->ccc1re, r->ccc1im, r->cc1re, r->cc1im, GMP_RNDN);
      cmpfr_abs2(r->delta1re, r->delta1re, r->delta1im, r->tmp0, r->tmp1, GMP_RNDN);
      // delta2 := cc2 - ccc2
      cmpfr_sub(r->delta2re, r->delta2im, r->ccc2re, r->ccc2im, r->cc2re, r->cc2im, GMP_RNDN);
      cmpfr_abs2(r->delta2re, r->delta2re, r->delta2im, r->tmp0, r->tmp1, GMP_RNDN);

    } while (mpfr_greater_p(r->delta1re, r->eps_2) && mpfr_greater_p(r->delta2re, r->eps_2) && --limit);
    // delta1 := |cc1 - c|^2
    cmpfr_sub(r->delta1re, r->delta1im, r->cc1re, r->cc1im, r->cre, r->cim, GMP_RNDN);
    cmpfr_abs2(r->delta1re, r->delta1re, r->delta1im, r->tmp0, r->tmp1, GMP_RNDN);
    // delta2 := |cc2 - c|^2
    cmpfr_sub(r->delta2re, r->delta2im, r->cc2re, r->cc2im, r->cre, r->cim, GMP_RNDN);
    cmpfr_abs2(r->delta2re, r->delta2re, r->delta2im, r->tmp0, r->tmp1, GMP_RNDN);
    // delta1 < delta2
    if (mpfr_less_p(r->delta1re, r->delta2re)) {
      r->bit_to_push = 0;
      // c := c1
      cmpfr_set(r->cre, r->cim, r->cc1re, r->cc1im, GMP_RNDN);
    } else {
      r->bit_to_push = 1;
      // c := c2
      cmpfr_set(r->cre, r->cim, r->cc2re, r->cc2im, GMP_RNDN);
    }
  } else {
    r->bit_to_push = -1;
    // c := c0
    cmpfr_set(r->cre, r->cim, r->cc0re, r->cc0im, GMP_RNDN);
  }
  // eps := |oldc - c|^2 / 2^(2*accuracy)
  cmpfr_sub(r->deltare, r->deltaim, r->oldcre, r->oldcim, r->cre, r->cim, GMP_RNDN);
  cmpfr_abs2(r->eps_2, r->deltare, r->deltaim, r->tmp0, r->tmp1, GMP_RNDN);
  mpfr_div_2si(r->eps_2, r->eps_2, 2 * r->accuracy, GMP_RNDN);
  // n, z, d := iterate z -> z^2 + c with z0 = 0
  r->oldn = r->n;
  r->n = 0;
  mpfr_set_si(r->zre, 0, GMP_RNDN);
  mpfr_set_si(r->zim, 0, GMP_RNDN);
  mpfr_set_si(r->zre2, 0, GMP_RNDN);
  mpfr_set_si(r->zim2, 0, GMP_RNDN);
  mpfr_set_si(r->zmag2, 0, GMP_RNDN);
  while (mpfr_get_d(r->zmag2, GMP_RNDN) < r->escape_radius_2) {
    r->n = r->n + 1;
    mpfr_mul(r->zim, r->zre, r->zim, GMP_RNDN);
    mpfr_mul_2si(r->zim, r->zim, 1, GMP_RNDN);
    mpfr_sub(r->zre, r->zre2, r->zim2, GMP_RNDN);
    mpfr_add(r->zre, r->zre, r->cre, GMP_RNDN);
    mpfr_add(r->zim, r->zim, r->cim, GMP_RNDN);
    mpfr_sqr(r->zre2, r->zre, GMP_RNDN);
    mpfr_sqr(r->zim2, r->zim, GMP_RNDN);
    mpfr_add(r->zmag2, r->zre2, r->zim2, GMP_RNDN);
  }
  r->d = dwell(r->log_escape_radius_2, r->n, mpfr_get_d(r->zmag2, GMP_RNDN));
  return 1;
}

extern void mandelbrot_external_ray_out_delete(struct mandelbrot_external_ray_out *r) {
  mpfr_clear(r->eps_2);
  mpfr_clear(r->cre);
  mpfr_clear(r->cim);
  mpfr_clear(r->zre);
  mpfr_clear(r->zim);
  mpfr_clear(r->zre2);
  mpfr_clear(r->zim2);
  mpfr_clear(r->zmag2);
  mpfr_clear(r->cccre);
  mpfr_clear(r->cccim);
  mpfr_clear(r->ccre);
  mpfr_clear(r->ccim);
  mpfr_clear(r->tmp0);
  mpfr_clear(r->tmp1);
  mpfr_clear(r->tmp2);
  mpfr_clear(r->tmp3);
  mpfr_clear(r->wre);
  mpfr_clear(r->wim);
  mpfr_clear(r->dcre);
  mpfr_clear(r->dcim);
  mpfr_clear(r->deltare);
  mpfr_clear(r->deltaim);
  mpfr_clear(r->ccc1re);
  mpfr_clear(r->ccc1im);
  mpfr_clear(r->ccc2re);
  mpfr_clear(r->ccc2im);
  mpfr_clear(r->cc1re);
  mpfr_clear(r->cc1im);
  mpfr_clear(r->cc2re);
  mpfr_clear(r->cc2im);
  mpfr_clear(r->w1re);
  mpfr_clear(r->w1im);
  mpfr_clear(r->dc1re);
  mpfr_clear(r->dc1im);
  mpfr_clear(r->w2re);
  mpfr_clear(r->w2im);
  mpfr_clear(r->dc2re);
  mpfr_clear(r->dc2im);
  mpfr_clear(r->delta1re);
  mpfr_clear(r->delta1im);
  mpfr_clear(r->delta2re);
  mpfr_clear(r->delta2im);
  mpfr_clear(r->cc0re);
  mpfr_clear(r->cc0im);
  mpfr_clear(r->oldcre);
  mpfr_clear(r->oldcim);
  free(r);
}

extern int mandelbrot_external_ray_out_have_bit(struct mandelbrot_external_ray_out *r) {
  return r->n < r->oldn && 0 <= r->bit_to_push;
}

extern int mandelbrot_external_ray_out_get_bit(struct mandelbrot_external_ray_out *r) {
  return r->bit_to_push;
}

extern void mandelbrot_external_ray_out_get(struct mandelbrot_external_ray_out *r, mpfr_t x, mpfr_t y) {
  mpfr_set_prec(x, mpfr_get_prec(r->cre));
  mpfr_set(x, r->cre, GMP_RNDN);
  mpfr_set_prec(y, mpfr_get_prec(r->cim));
  mpfr_set(y, r->cim, GMP_RNDN);
}
