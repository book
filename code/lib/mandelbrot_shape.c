#include <complex.h>
#include "mandelbrot_shape.h"
#include "mpfr_complex.h"

static double cabs2(complex double z) {
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

extern enum mandelbrot_shape mandelbrot_shape(const mpfr_t cx, const mpfr_t cy, int period) {
  enum mandelbrot_shape retval;
#define VARS zx, zy, dcx, dcy, dzx, dzy, dcdcx, dcdcy, dcdzx, dcdzy, t0, t1, t2, t3, t4, t5
  mpfr_t VARS;
  mpfr_inits2(mpfr_get_prec(cx), VARS, (mpfr_ptr) 0);

  cmpfr_set(zx, zy, cx, cy, GMP_RNDN);
  cmpfr_set_si(dcx, dcy, 1, 0, GMP_RNDN);
  cmpfr_set_si(dzx, dzy, 1, 0, GMP_RNDN);
  cmpfr_set_si(dcdcx, dcdcy, 0, 0, GMP_RNDN);
  cmpfr_set_si(dcdzx, dcdzy, 0, 0, GMP_RNDN);
  for (int i = 1; i < period; ++i) {
    // dcdc := 2 (z dcdc + dc^2)
    cmpfr_mul(dcdcx, dcdcy, dcdcx, dcdcy, zx, zy, t0, t1, t2, t3, GMP_RNDN);
    cmpfr_sqr(t2, t3, dcx, dcy, t0, t1, GMP_RNDN);
    cmpfr_add(dcdcx, dcdcy, dcdcx, dcdcy, t2, t3, GMP_RNDN);
    cmpfr_mul_2si(dcdcx, dcdcy, dcdcx, dcdcy, 1, GMP_RNDN);
    // dcdz := 2 (z dcdz + dc dz)
    cmpfr_mul(dcdzx, dcdzy, dcdzx, dcdzy, zx, zy, t0, t1, t2, t3, GMP_RNDN);
    cmpfr_mul(t4, t5, dcx, dcy, dzx, dzy, t0, t1, t2, t3, GMP_RNDN);
    cmpfr_add(dcdzx, dcdzy, dcdzx, dcdzy, t4, t5, GMP_RNDN);
    cmpfr_mul_2si(dcdzx, dcdzy, dcdzx, dcdzy, 1, GMP_RNDN);
    // dc := 2 z dc + 1
    cmpfr_mul(dcx, dcy, dcx, dcy, zx, zy, t0, t1, t2, t3, GMP_RNDN);
    cmpfr_mul_2si(dcx, dcy, dcx, dcy, 1, GMP_RNDN);
    mpfr_add_d(dcx, dcx, 1.0, GMP_RNDN);
    // dz := 2 z dz
    cmpfr_mul(dzx, dzy, dzx, dzy, zx, zy, t0, t1, t2, t3, GMP_RNDN);
    cmpfr_mul_2si(dzx, dzy, dzx, dzy, 1, GMP_RNDN);
    // z := z^2 + c
    cmpfr_sqr(zx, zy, zx, zy, t0, t1, GMP_RNDN);
    cmpfr_add(zx, zy, zx, zy, cx, cy, GMP_RNDN);
  }
  // e := - (dcdc/dc/2 + dcdz/dz)/(dc dz)
  cmpfr_div(dcdcx, dcdcy, dcdcx, dcdcy, dcx, dcy, t0, t1, t2, t3, GMP_RNDN);
  cmpfr_mul_2si(dcdcx, dcdcy, dcdcx, dcdcy, -1, GMP_RNDN);
  cmpfr_div(dcdzx, dcdzy, dcdzx, dcdzy, dzx, dzy, t0, t1, t2, t3, GMP_RNDN);
  cmpfr_add(dcdcx, dcdcy, dcdcx, dcdcy, dcdzx, dcdzy, GMP_RNDN);
  cmpfr_mul(dcx, dcy, dcx, dcy, dzx, dzy, t0, t1, t2, t3, GMP_RNDN);
  cmpfr_div(dcdcx, dcdcy, dcdcx, dcdcy, dcx, dcy, t0, t1, t2, t3, GMP_RNDN);
  double ex = -mpfr_get_d(dcdcx, GMP_RNDN);
  double ey = -mpfr_get_d(dcdcy, GMP_RNDN);
  complex double e = ex + I * ey;
  double d0 = cabs2(e);
  double d1 = cabs2(e - 1.0);
  if (d0 < d1) {
    retval = mandelbrot_shape_cardioid;
  } else {
    retval = mandelbrot_shape_circle;
  }
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
  return retval;
}
