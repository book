#include "mandelbrot_wucleus.h"
#include "mpfr_complex.h"

extern int mandelbrot_wucleus(mpfr_t zre_out, mpfr_t zim_out, mpfr_t dzre_out, mpfr_t dzim_out, const mpfr_t z0re, const mpfr_t z0im, const mpfr_t cre, const mpfr_t cim, int period, const mpfr_t epsilon, int max_iters) {
  int retval = 0;
  int bits = mpfr_get_prec(cre);
#define VARS eps, zzre, zzim, zre, zim, dzre, dzim, zz1re, zz1im, dz1re, dz1im, d, t0, t1, t2, t3
  mpfr_t VARS;
  mpfr_inits2(bits, VARS, (mpfr_ptr) 0);
  mpfr_sqr(eps, epsilon, GMP_RNDN);
  mpfr_div_d(eps, eps, 65536.0, GMP_RNDN);
  // zz := z0
  cmpfr_set(zzre, zzim, z0re, z0im, GMP_RNDN);
  for (int j = 0; j < max_iters; ++j) {
    // z := zz
    cmpfr_set(zre, zim, zzre, zzim, GMP_RNDN);
    // dz := 1
    cmpfr_set_si(dzre, dzim, 1, 0, GMP_RNDN);
    for (int i = 0; i < period; ++i) {
      // dz := 2 * z * dz
      cmpfr_mul(dzre, dzim, zre, zim, dzre, dzim, t0, t1, t2, t3, GMP_RNDN);
      cmpfr_mul_2si(dzre, dzim, dzre, dzim, 1, GMP_RNDN);
      // z := z^2 + c
      cmpfr_sqr(zre, zim, zre, zim, t0, t1, GMP_RNDN);
      cmpfr_add(zre, zim, zre, zim, cre, cim, GMP_RNDN);
    }
    // zz1 := zz - (z  - zz) / (dz - 1)
    cmpfr_sub(zz1re, zz1im, zre, zim, zzre, zzim, GMP_RNDN);
    cmpfr_sub_d(dz1re, dz1im, dzre, dzim, 1.0, 0.0, GMP_RNDN);
    cmpfr_div(zz1re, zz1im, zz1re, zz1im, dz1re, dz1im, t0, t1, t2, t3, GMP_RNDN);
    cmpfr_sub(zz1re, zz1im, zzre, zzim, zz1re, zz1im, GMP_RNDN);
    // d := |zz1 - zz|^2
    cmpfr_sub(t0, t1, zz1re, zz1im, zzre, zzim, GMP_RNDN);
    cmpfr_abs2(d, t0, t1, t2, t3, GMP_RNDN);
    if (mpfr_less_p(d, eps)) {
      mpfr_set_prec(zre_out, bits);
      mpfr_set_prec(zim_out, bits);
      mpfr_set_prec(dzre_out, bits);
      mpfr_set_prec(dzim_out, bits);
      cmpfr_set(zre_out, zim_out, zre, zim, GMP_RNDN);
      cmpfr_set(dzre_out, dzim_out, dzre, dzim, GMP_RNDN);
      retval = 1;
      break;
    }
    // zz := zz1
    cmpfr_set(zzre, zzim, zz1re, zz1im, GMP_RNDN);
  }
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
  return retval;
}
