#include <stdlib.h>
#include <string.h>

#include "mandelbrot_binary_angle.h"

struct mandelbrot_binary_angle {
  int preperiod;
  int period;
  char *pre;
  char *per;
};

// FIXME check canonical form, ie no .01(01) etc
extern struct mandelbrot_binary_angle *mandelbrot_binary_angle_from_string(const char *s) {
  const char *dot = strchr(s,   '.');
  if (! dot) { return 0; }
  const char *bra = strchr(dot, '(');
  if (! bra) { return 0; }
  const char *ket = strchr(bra, ')');
  if (! ket) { return 0; }
  if (s[0] != '.') { return 0; }
  if (ket[1] != 0) { return 0; }
  struct mandelbrot_binary_angle *a = malloc(sizeof(struct mandelbrot_binary_angle));
  a->preperiod = bra - dot - 1;
  a->period = ket - bra - 1;
  if (a->period < 1) {
    free(a);
    return 0;
  }
  a->pre = malloc(a->preperiod + 1);
  a->per = malloc(a->period + 1);
  memcpy(a->pre, dot + 1, a->preperiod);
  memcpy(a->per, bra + 1, a->period);
  a->pre[a->preperiod] = 0;
  a->per[a->period] = 0;
  for (int i = 0; i < a->preperiod; ++i) {
    if (a->pre[i] != '0' && a->pre[i] != '1') {
      mandelbrot_binary_angle_delete(a);
      return 0;
    }
  }
  for (int i = 0; i < a->period; ++i) {
    if (a->per[i] != '0' && a->per[i] != '1') {
      mandelbrot_binary_angle_delete(a);
      return 0;
    }
  }
  return a;
}

extern struct mandelbrot_binary_angle *mandelbrot_binary_angle_from_rational(const mpq_t r) {
  mpq_t q0, q, one, two;
  mpq_init(q0);
  mpq_set(q0, r);
  mpq_canonicalize(q0);
  if (mpq_sgn(q0) < 0 || mpq_cmp_si(q0, 1, 1) > 0) {
    mpq_clear(q0);
    return 0;
  }
  if (mpq_cmp_si(q0, 1, 1) == 0) {
    mpq_set_si(q0, 0, 1);
  }
  mpq_init(q);
  mpq_init(one);
  mpq_set_si(one, 1, 1);
  mpq_init(two);
  mpq_set_si(two, 2, 1);
  struct mandelbrot_binary_angle *a = malloc(sizeof(struct mandelbrot_binary_angle));
  a->preperiod = mpz_scan1(mpq_denref(q0), 0);
  a->pre = malloc(a->preperiod + 1);
  for (int i = 0; i < a->preperiod; ++i) {
    int bit = mpq_cmp_si(q0, 1, 2) >= 0;
    mpq_mul(q0, q0, two);
    if (bit) {
      mpq_sub(q0, q0, one);
    }
    a->pre[i] = '0' + bit;
  }
  a->pre[a->preperiod] = 0;
  int period = 0;
  mpq_set(q, q0);
  do {
    int bit = mpq_cmp_si(q, 1, 2) >= 0;
    mpq_mul(q, q, two);
    if (bit) {
      mpq_sub(q, q, one);
    }
    period++;
  } while (! mpq_equal(q0, q));
  a->period = period;
  a->per = malloc(a->period + 1);
  mpq_set(q, q0);
  for (int i = 0; i < a->period; ++i) {
    int bit = mpq_cmp_si(q, 1, 2) >= 0;
    mpq_mul(q, q, two);
    if (bit) {
      mpq_sub(q, q, one);
    }
    a->per[i] = '0' + bit;
  }
  a->per[a->period] = 0;
  mpq_clear(q0);
  mpq_clear(q);
  mpq_clear(one);
  mpq_clear(two);
  return a;
}

extern void mandelbrot_binary_angle_delete(struct mandelbrot_binary_angle *a) {
  free(a->pre);
  free(a->per);
  free(a);
}

extern char *mandelbrot_binary_angle_to_string(const struct mandelbrot_binary_angle *t) {
  int length = 1 + t->preperiod + 1 + t->period + 1;
  char *s = malloc(length + 1);
  s[0] = 0;
  strncat(s, ".", length);
  strncat(s, t->pre, length);
  strncat(s, "(", length);
  strncat(s, t->per, length);
  strncat(s, ")", length);
  return s;
}

extern void mandelbrot_binary_angle_to_rational(mpq_t r, const struct mandelbrot_binary_angle *t) {
  mpq_t pre, per;
  mpq_init(pre);
  mpq_init(per);
  mpz_set_str(mpq_numref(pre), t->pre, 2);
  mpz_set_si(mpq_denref(pre), 1);
  mpz_mul_2exp(mpq_denref(pre), mpq_denref(pre), t->preperiod);
  mpq_canonicalize(pre);
  mpz_set_str(mpq_numref(per), t->per, 2);
  mpz_set_si(mpq_denref(per), 1);
  mpz_mul_2exp(mpq_denref(per), mpq_denref(per), t->period);
  mpz_sub_ui(mpq_denref(per), mpq_denref(per), 1);
  mpz_mul_2exp(mpq_denref(per), mpq_denref(per), t->preperiod);
  mpq_canonicalize(per);
  mpq_add(r, pre, per);
  mpq_clear(pre);
  mpq_clear(per);
}

extern struct mandelbrot_binary_angle *mandelbrot_binary_angle_canonicalize(const struct mandelbrot_binary_angle *t) {
  mpq_t r;
  mpq_init(r);
  mandelbrot_binary_angle_to_rational(r, t);
  struct mandelbrot_binary_angle *a = mandelbrot_binary_angle_from_rational(r);
  mpq_clear(r);
  return a;
}

static bool mandelbrot_binary_angle_equal(const struct mandelbrot_binary_angle *a, const struct mandelbrot_binary_angle *b) {
  if (a->preperiod != b->preperiod) { return false; }
  if (a->period != b->period) { return false; }
  if (strcmp(a->pre, b->pre)) { return false; }
  if (strcmp(a->per, b->per)) { return false; }
  return true;
}

extern bool mandelbrot_binary_angle_is_canonical(const struct mandelbrot_binary_angle *a) {
  struct mandelbrot_binary_angle *b = mandelbrot_binary_angle_canonicalize(a);
  bool result = mandelbrot_binary_angle_equal(a, b);
  mandelbrot_binary_angle_delete(b);
  return result;
}

extern int mandelbrot_binary_angle_preperiod(const struct mandelbrot_binary_angle *t) {
  return t->preperiod;
}

extern int mandelbrot_binary_angle_period(const struct mandelbrot_binary_angle *t) {
  return t->period;
}

// FIXME gives bad output for non-canonical input like .1(1)
extern struct mandelbrot_binary_angle *mandelbrot_binary_angle_other_representation(const struct mandelbrot_binary_angle *t) {
  if (t->period != 1) { return 0; }
  struct mandelbrot_binary_angle *a = malloc(sizeof(struct mandelbrot_binary_angle));
  a->preperiod = t->preperiod;
  a->period = t->period;
  a->pre = malloc(a->preperiod + 1);
  a->per = malloc(a->period + 1);
  memcpy(a->pre, t->pre, a->preperiod + 1);
  memcpy(a->per, t->per, a->period + 1);
  if (a->per[0] == '0') {
    a->per[0] = '1';
    if (a->preperiod > 0) {
      a->pre[a->preperiod - 1] = '0';
    }
  } else {
    a->per[0] = '0';
    if (a->preperiod > 0) {
      a->pre[a->preperiod - 1] = '1';
    }
  }
  return a;
}

extern struct mandelbrot_binary_angle *mandelbrot_binary_angle_tune(const struct mandelbrot_binary_angle *lo, const struct mandelbrot_binary_angle *hi, const struct mandelbrot_binary_angle *t) {
  if (lo->preperiod > 0) { return 0; }
  if (hi->preperiod > 0) { return 0; }
  if (lo->period != hi->period) { return 0; }
  struct mandelbrot_binary_angle *a = malloc(sizeof(struct mandelbrot_binary_angle));
  a->preperiod = lo->period * t->preperiod;
  a->period = lo->period * t->period;
  a->pre = malloc(a->preperiod + 1);
  a->per = malloc(a->period + 1);
  a->pre[0] = 0;
  a->per[0] = 0;
  for (int i = 0; i < t->preperiod; ++i) {
    if (t->pre[i] == '0') {
      strncat(a->pre, lo->per, a->preperiod);
    } else {
      strncat(a->pre, hi->per, a->preperiod);
    }
  }
  for (int i = 0; i < t->period; ++i) {
    if (t->per[i] == '0') {
      strncat(a->per, lo->per, a->period);
    } else {
      strncat(a->per, hi->per, a->period);
    }
  }
  return a;
}

extern void mandelbrot_binary_angle_bulb(struct mandelbrot_binary_angle **lo, struct mandelbrot_binary_angle **hi, const mpq_t b) {
/*
bulb' :: InternalAngle -> BinaryAnglePair
bulb' (InternalAngle pq) = BinaryAnglePair (BinaryAngle [] ls) (BinaryAngle [] us)
  where
    q = denominator (w pq)
    ls = is ++ [False, True]
    us = is ++ [True, False]
    is = genericTake (q - 2) . map i . iterate r $ pq
    i a = not $ 0 < a && a < 1 - pq
    r a = w (a + pq)
    w a
      | f < 0 = 1 + f
      | otherwise = f
      where
        (_, f) = properFraction a :: (Integer, Rational)
*/
  *lo = malloc(sizeof(struct mandelbrot_binary_angle));
  *hi = malloc(sizeof(struct mandelbrot_binary_angle));
  (*lo)->preperiod = 0;
  (*hi)->preperiod = 0;
  (*lo)->period = mpz_get_si(mpq_denref(b));
  (*hi)->period = (*lo)->period;
  (*lo)->pre = malloc((*lo)->preperiod + 1);
  (*lo)->per = malloc((*lo)->period + 1);
  (*hi)->pre = malloc((*hi)->preperiod + 1);
  (*hi)->per = malloc((*hi)->period + 1);
  (*lo)->pre[0] = 0;
  (*lo)->per[0] = 0;
  (*hi)->pre[0] = 0;
  (*hi)->per[0] = 0;
  mpq_t a, up, one;
  mpq_init(a);
  mpq_init(up);
  mpq_init(one);
  mpq_set_si(one, 1, 1);
  mpq_set(a, b);
  mpq_set(up, one);
  mpq_sub(up, up, b);
  for (int i = 0; i < (*lo)->period - 2; ++i) {
    int k = ! (0 < mpq_sgn(a) && mpq_cmp(a, up) < 0);
    strncat((*lo)->per, k ? "1" : "0", (*lo)->period);
    strncat((*hi)->per, k ? "1" : "0", (*hi)->period);
    mpq_add(a, a, b);
    if (mpq_cmp(a, one) >= 0) {
      mpq_sub(a, a, one);
    }
  }
  strncat((*lo)->per, "01", (*lo)->period);
  strncat((*hi)->per, "10", (*hi)->period);
  mpq_clear(a);
  mpq_clear(up);
  mpq_clear(one);
}
