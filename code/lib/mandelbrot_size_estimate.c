#include "mandelbrot_size_estimate.h"
#include "mpfr_complex.h"

extern void mandelbrot_size_estimate(mpfr_t sx, mpfr_t sy, const mpfr_t cx, const mpfr_t cy, int period) {
  mpfr_t onex, oney, zx, zy, lx, ly, bx, by, dx, dy, s, t, u, v;
  mpfr_inits2(mpfr_get_prec(cx), onex, oney, zx, zy, lx, ly, bx, by, dx, dy, s, t, u, v, (mpfr_ptr) 0);
  // one = 1
  mpfr_set_si(onex, 1, GMP_RNDN);
  mpfr_set_si(oney, 0, GMP_RNDN);
  // l = 1
  mpfr_set_si(lx, 1, GMP_RNDN);
  mpfr_set_si(ly, 0, GMP_RNDN);
  // b = 1
  mpfr_set_si(bx, 1, GMP_RNDN);
  mpfr_set_si(by, 0, GMP_RNDN);
  // z = 0
  mpfr_set_si(zx, 0, GMP_RNDN);
  mpfr_set_si(zy, 0, GMP_RNDN);
  for (int i = 1; i < period; ++i) {
    // z = z^2 + c
    mpfr_sqr(u, zx, GMP_RNDN);
    mpfr_sqr(v, zy, GMP_RNDN);
    mpfr_mul(zy, zx, zy, GMP_RNDN);
    mpfr_sub(zx, u, v, GMP_RNDN);
    mpfr_mul_2ui(zy, zy, 1, GMP_RNDN);
    mpfr_add(zx, zx, cx, GMP_RNDN);
    mpfr_add(zy, zy, cy, GMP_RNDN);
    // l = 2 z l
    cmpfr_mul(lx, ly, lx, ly, zx, zy, s, t, u, v, GMP_RNDN);
    cmpfr_mul_2si(lx, ly, lx, ly, 1, GMP_RNDN);
    // b = b + 1/l
    cmpfr_div(dx, dy, onex, oney, lx, ly, s, t, u, v, GMP_RNDN);
    cmpfr_add(bx, by, bx, by, dx, dy, GMP_RNDN);
  }
  // size = 1 / (b l^2)
  cmpfr_sqr(lx, ly, lx, ly, u, v, GMP_RNDN);
  cmpfr_mul(dx, dy, bx, by, lx, ly, s, t, u, v, GMP_RNDN);
  cmpfr_div(sx, sy, onex, oney, dx, dy, s, t, u, v, GMP_RNDN);
  mpfr_clears(onex, oney, zx, zy, lx, ly, bx, by, dx, dy, s, t, u, v, (mpfr_ptr) 0);
}
