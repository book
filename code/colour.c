#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stream.h"
#include "hsv2rgb.h"

void colour(int *r, int *g, int *b, int final_n, float final_z_abs, float final_z_arg, float de, int final_p, int t, int theme) {
  float continuous_escape_time = final_n - log2f(final_z_abs + 1.0f);
  (void) continuous_escape_time;
  float radius = 0.0f, angle = 0.0f;
  if (t > 0) {
    radius = final_z_abs;
    angle = final_z_arg;
  } else if (t < 0) {
    radius = -log2f(1.0f - final_z_abs);
    angle = fmodf(powf(2.0, floorf(radius) + 3.0f) * final_z_arg, 1.0f);
    radius = 1.0 - fmodf(radius, 1.0f);
  }
  float k = powf(0.5f, 0.5f - radius);
  float grid_weight = 0.05f;
  int grid =
    grid_weight     < radius && radius < 1.0f - grid_weight &&
    grid_weight * k < angle  && angle  < 1.0f - grid_weight * k;
  float hue = (final_p - 1.0f) / 24.618033988749895f;
  float sat = final_p > 0.0f ? 0.5f : 0.0f;
  float val = fmin(tanh(fmin(fmax(de, 0.0), 4.0)), 0.9 + 0.1 * grid);
  if (t == 0) {
    hue = 0.0f;
    sat = 0.0f;
    val = 0.0f;
  }
  if (theme == 1) {
    hue = 0.0;
    sat = 0.5 * (1.0 - tanh(fmin(fmax(de, 0), 4)));
    val = 0.5 + 0.5 * val;
  }
  float red, grn, blu;
  hsv2rgb(hue, sat, val, &red, &grn, &blu);
  *r = channel(red);
  *g = channel(grn);
  *b = channel(blu);
}

int cmp_int(int a, int b) {
  if (a > b) return  1;
  if (a < b) return -1;
  return 0;
}

int stream_compatible(struct stream *s, struct stream *t) {
  return s->width == t->width && s->height == t->height;
}

struct stream *stream_new_for_stem(char *stem, char *suffix) {
  int length = strlen(stem) + strlen(suffix) + 100;
  char *name = calloc(length, 1);
  snprintf(name, length, "%s_%s.ppm", stem, suffix);
  struct stream *s = stream_new(name);
  free(name);
  return s;
}

int main(int argc, char **argv) {
  int retval = 1;
  char *stem = "out";
  int theme = 0;
  if (argc > 1) {
    stem = argv[1];
  }
  if (argc > 2) {
    theme = atoi(argv[2]);
  }

  struct stream *s_n = stream_new_for_stem(stem, "n");
  struct stream *s_t = stream_new_for_stem(stem, "t");
  struct stream *s_r = stream_new_for_stem(stem, "z_abs");
  struct stream *s_a = stream_new_for_stem(stem, "z_arg");
  struct stream *s_d = stream_new_for_stem(stem, "de");
  struct stream *s_p = stream_new_for_stem(stem, "p");
  if (s_n && s_t && s_r && s_a && s_d && s_p) {
    if (stream_compatible(s_n, s_t) &&
        stream_compatible(s_n, s_r) &&
        stream_compatible(s_n, s_a) &&
        stream_compatible(s_n, s_d) &&
        stream_compatible(s_n, s_p)) {

      int width = s_n->width;
      int height = s_n->height;
      printf("P6\n%d %d\n255\n", width, height);
      for (int j = 0; j < height; ++j) {
        for (int i = 0; i < width; ++i) {
          int   n = stream_read(s_n);
          int   t = cmp_int(stream_read(s_t) & 255, 128);
          float r = stream_read(s_r) / (float) (1 << 24);
          float a = stream_read(s_a) / (float) (1 << 24);
          float d = stream_read(s_d) / (float) (1 << 24) * (float) (1 << 11);
          int   p = stream_read(s_p);
          int red, grn, blu;
          colour(&red, &grn, &blu, n, r, a, d, p, t, theme);
          putchar(red);
          putchar(grn);
          putchar(blu);
        }
      }
      retval = 0;

    }
  }
  if (s_n) { stream_free(s_n); }
  if (s_t) { stream_free(s_t); }
  if (s_r) { stream_free(s_r); }
  if (s_a) { stream_free(s_a); }
  if (s_d) { stream_free(s_d); }
  if (s_p) { stream_free(s_p); }

  return retval;
}
