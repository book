#ifndef HSV2RGB_H
#define HSV2RGB_H 1

int channel(float c);
void hsv2rgb(float h, float s, float v, float *red, float *grn, float *blu);

#endif
