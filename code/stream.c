#include <stdio.h>
#include <stdlib.h>

#include "stream.h"

struct stream *stream_new(char *name) {
  struct stream *s = calloc(1, sizeof(*s));
  if (! s) {
    return 0;
  }
  s->file = fopen(name, "rb");
  if (! s->file) {
    free(s);
    return 0;
  }
  if (2 != fscanf(s->file, "P6 %d %d 255", &s->width, &s->height)) {
    fclose(s->file);
    free(s);
    return 0;
  }
  if (fgetc(s->file) != '\n') {
    fclose(s->file);
    free(s);
    return 0;
  }
  return s;
}

void stream_free(struct stream *s) {
  fclose(s->file);
  free(s);
}

int stream_read(struct stream *s) {
  int r = fgetc(s->file);
  int g = fgetc(s->file);
  int b = fgetc(s->file);
  return (r << 16) | (g << 8) | b;
}
