#define _POSIX_C_SOURCE 200809L
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <cairo/cairo.h>

#include "stream.h"

int main(int argc, char **argv) {
  int retval = 1;
  char *in = "in.ppm";
  char *out = "out.png";
  if (argc > 1) { in  = argv[1]; }
  if (argc > 2) { out = argv[2]; }
  struct stream *s = stream_new(in);
  if (s) {
    cairo_surface_t *c = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, s->width, s->height);
    if (c) {
      uint32_t *data = (uint32_t *) cairo_image_surface_get_data(c);
      int channels = 4;
      int stride = cairo_image_surface_get_stride(c) / channels;
      for (int j = 0; j < s->height; ++j) {
        for (int i = 0; i < s->width; ++i) {
          int p = stream_read(s);
          int k = j * stride + i;
          data[k] = (0xFF << 24) | p;
        }
      }
      cairo_surface_mark_dirty(c);
      double aspect = s->width / (double) s->height;
      cairo_t *ctx = cairo_create(c);
      cairo_translate(ctx, s->width / 2.0, s->height / 2.0);
      cairo_scale(ctx, s->height / 2.0, -s->height / 2.0);
      cairo_set_line_width(ctx, 4.0 / s->height);
      cairo_matrix_t fontmatrix;
      cairo_matrix_init_scale(&fontmatrix, 0.06, -0.06);
      cairo_set_font_matrix(ctx, &fontmatrix);
      cairo_select_font_face(ctx, "LM Sans 10", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
      char *line = 0;
      size_t linelen = 0;
      while (1 < getline(&line, &linelen, stdin)) {
        if (strncmp(line, "line ", strlen("line ")) == 0 || strncmp(line, "fill ", strlen("fill ")) == 0) {
          line[strlen(line) - 1] = 0;
          FILE *f = popen(line + 5, "r");
          if (f) {
            fprintf(stderr, "%s\n", line);
            int first = 1;
            double x, y, oldx = 0, oldy = 0;
            int oldinbounds = 0;
            while (2 == fscanf(f, "%lf %lf\n", &x, &y)) {
              int inbounds = -aspect <= x && x <= aspect && -1 <= y && y <= 1;
              if (oldinbounds) {
                cairo_line_to(ctx, x, y);
              } else {
                if (inbounds) {
                  if (first) {
                    cairo_move_to(ctx, x, y);
                  } else {
                    cairo_move_to(ctx, oldx, oldy);
                    cairo_line_to(ctx, x, y);
                  }
                } else {
                  // segment out of bounds
                }
              }
              oldinbounds = inbounds;
              oldx = x;
              oldy = y;
              first = 0;
            }
            if (strncmp(line, "line ", strlen("line ")) == 0) {
              cairo_stroke(ctx);
            } else {
              cairo_fill(ctx);
            }
            pclose(f);
          }
        } else if (strncmp(line, "rgba ", strlen("rgba ")) == 0) {
          double r, g, b, a;
          if (4 == sscanf(line + 5, "%lf %lf %lf %lf\n", &r, &g, &b, &a)) {
            cairo_set_source_rgba(ctx, r, g, b, a);
            fprintf(stderr, "rgba %f %f %f %f\n", r, g, b, a);
          }
        } else if (strncmp(line, "text ", strlen("text ")) == 0) {
          line[strlen(line) - 1] = 0;
          double x, y;
          int n;
          if (2 <= sscanf(line + 5, "%lf %lf %n", &x, &y, &n)) {
            fprintf(stderr, "text %f %f %s\n", x, y, line + 5 + n);
            cairo_text_extents_t e;
            cairo_text_extents(ctx, line + 5 + n, &e);
            cairo_move_to(ctx, x - e.width / 2.0, y - e.height / 2.0);
            cairo_show_text(ctx, line + 5 + n);
            cairo_fill(ctx);
          }
        }
      }
      cairo_destroy(ctx);
      cairo_surface_write_to_png(c, out);
      retval = 0;
      cairo_surface_destroy(c);
    }
    stream_free(s);
  }
  return retval;
}
