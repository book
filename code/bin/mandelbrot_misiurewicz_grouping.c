#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct distance {
  double distance;
  int index;
};

int cmp_distance(const void *a, const void *b) {
  const struct distance *x = a;
  const struct distance *y = b;
  if (x->distance < y->distance) return -1;
  if (x->distance > y->distance) return 1;
  return 0;
}

struct item {
  double re;
  double im;
  char *ea;
  bool out;
};

int main(int argc, char **argv) {
  if (argc < 2) {
    return 1;
  }
  int preperiod = -1;
  int period = -1;
  if (2 != sscanf(argv[1], "landing_%d_%d.txt", &preperiod, &period)) {
    return 1;
  }
  if (! (preperiod >= 0 && period > 0)) {
    return 1;
  }
  char outname[1024];
  snprintf(outname, 1000, "misiurewicz_%d_%d.txt", preperiod, period);
  FILE *in = fopen(argv[1], "rb");
  if (! in) {
    return 1;
  }
  FILE *out = fopen(outname, "wb");
  if (! out) {
    return 1;
  }
  printf("%s\n", outname);
  // count input lines
  int count = 0;
  double tmp0, tmp1;
  char *tmp2 = 0;
  while (3 == fscanf(in, "%lf %lf %ms\n", &tmp0, &tmp1, &tmp2)) {
    if (tmp2) {
      free(tmp2);
      tmp2 = 0;
    }
    count++;
  }
  if (tmp2) {
    free(tmp2);
    tmp2 = 0;
  }
  fseek(in, 0, SEEK_SET);
  // read input items
  struct item *items = calloc(count, sizeof(struct item));
  for (int k = 0; k < count; ++k) {
    fscanf(in, "%lf %lf %ms\n", &items[k].re, &items[k].im, &items[k].ea);
  }
  // compute nearby landing points
  struct distance *distances = calloc(count, sizeof(struct distance));
  for (int i = 0; i < count; ++i) {
    if (! items[i].out) {
      for (int j = 0; j < count; ++j) {
        double re = items[i].re - items[j].re;
        double im = items[i].im - items[j].im;
        distances[j].distance = re * re + im * im;
        distances[j].index = j;
      }
      qsort(distances, count, sizeof(struct distance), cmp_distance);
      int n = 0;
      for (int k = 0; k < count && distances[k].distance < 1e-16; ++k) {
        n = k + 1;
      }
      fprintf(out, "%d", n);
      double re = 0;
      double im = 0;
      for (int k = 0; k < n; ++k) {
        re += items[distances[k].index].re;
        im += items[distances[k].index].im;
      }
      re /= n;
      im /= n;
      double r = 0;
      for (int k = 0; k < n; ++k) {
        double dx = items[distances[k].index].re - re;
        double dy = items[distances[k].index].im - im;
        double rr = dx * dx + dy * dy;
        if (rr > r) {
          r = rr;
        }
      }
      r = sqrt(r);
      fprintf(out, " %.18e %.18e %.18e", re, im, r);
      for (int k = 0; k < n; ++k) {
        fprintf(out, " %s", items[distances[k].index].ea);
        items[distances[k].index].out = true;
      }
      fprintf(out, "\n");
    }
  }
  fclose(in);
  fclose(out);
  free(distances);
  free(items);
  return 0;
}
