#include <math.h>
#include <stdio.h>
#include <mpfr.h>

#include "mandelbrot_box_period.h"
#include "mandelbrot_nucleus.h"
#include "mandelbrot_shape.h"

void usage(const char *progname) {
  fprintf(stderr, "usage: %s radius re im\nexample: %s 16 0 0\n", progname, progname);
}

int main(int argc, char **argv) {
  if (argc != 4) {
    usage(argv[0]);
    return 1;
  }
#define VARS cx, cy, cx2, cy2, dx, dy
#define VARS2 r_min, r, s
  mpfr_t VARS, VARS2;
  mpfr_inits2(53, VARS2, (mpfr_ptr) 0);
  mpfr_set_str(r_min, argv[1], 10, GMP_RNDN);
  mpfr_log2(r, r_min, GMP_RNDN);
  mpfr_prec_t bits = fmax(53, 16 - mpfr_get_d(r, GMP_RNDN));
  mpfr_inits2(bits, VARS, (mpfr_ptr) 0);
  mpfr_set_str(cx, argv[2], 10, GMP_RNDN);
  mpfr_set_str(cy, argv[3], 10, GMP_RNDN);
  mpfr_set_d(r, 256.0, GMP_RNDN);
  int p_old = 0;
  while (mpfr_greater_p(r, r_min)) {
    // search
    int p = mandelbrot_box_period(cx, cy, r, 100000);
    if (p > p_old) {
      // compute
      mandelbrot_nucleus(cx2, cy2, cx, cy, p, 100);
      enum mandelbrot_shape shape = mandelbrot_shape(cx2, cy2, p);
      // handle islands
      if (shape == mandelbrot_shape_cardioid) {
        // s = |c - c2|
        mpfr_sub(dx, cx, cx2, GMP_RNDN);
        mpfr_sub(dy, cy, cy2, GMP_RNDN);
        mpfr_sqr(dx, dx, GMP_RNDN);
        mpfr_sqr(dy, dy, GMP_RNDN);
        mpfr_add(s, dx, dy, GMP_RNDN);
        mpfr_sqrt(s, s, GMP_RNDN);
        mpfr_mul_2si(s, s, 1, GMP_RNDN);
        // output
        mpfr_printf("--view %Re %Re %Re\n", cx, cy, s);
        fflush(stdout);
        // shunt
        p_old = p;
      }
    }
    mpfr_mul_d(r, r, sqrt(0.5), GMP_RNDN);
  } 
  mpfr_clears(VARS, VARS2, (mpfr_ptr) 0);
#undef VARS
  return 0;
}
