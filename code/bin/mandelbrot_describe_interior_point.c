#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include "mandelbrot_binary_angle.h"
#include "mandelbrot_parent.h"
#include "mandelbrot_nucleus.h"
#include "mandelbrot_wucleus.h"
#include "mpfr_complex.h"

void usage(const char *progname) {
  fprintf(stderr, "usage: %s bits re im\nexample: %s 53 0 0\n", progname, progname);
}

int main(int argc, char **argv) {
  if (argc != 4) {
    usage(argv[0]);
    return 1;
  }
  int bits = atoi(argv[1]);
#define VARS zx, zy, cx, cy, wux, wuy, parentx, parenty, bondx, bondy, mz2, z2, epsilon, d2, t0, t1, one, z0x, z0y, dz0x, dz0y, childx, childy, rootx, rooty
  mpfr_t VARS;
  mpfr_inits2(bits, VARS, (mpfr_ptr) 0);
  mpfr_set_str(cx, argv[2], 10, GMP_RNDN);
  mpfr_set_str(cy, argv[3], 10, GMP_RNDN);
  // z := 0
  mpfr_set_si(zx, 0, GMP_RNDN);
  mpfr_set_si(zy, 0, GMP_RNDN);
  mpfr_set_d(mz2, 1.0/0.0, GMP_RNDN);
  mpfr_set_si(epsilon, 1, GMP_RNDN);
  mpfr_mul_2si(epsilon, epsilon, 16 - bits, GMP_RNDN);
  mpfr_set_si(one, 1, GMP_RNDN);
  mpq_t angle;
  mpq_init(angle);
  for (int p = 1; p < 65536; ++p) {
    // z := z^2 + c
    cmpfr_sqr(zx, zy, zx, zy, t0, t1, GMP_RNDN);
    cmpfr_add(zx, zy, zx, zy, cx, cy, GMP_RNDN);
    // z2 := |z|^2
    cmpfr_abs2(z2, zx, zy, t0, t1, GMP_RNDN);
    if (mpfr_less_p(z2, mz2)) {
      mpfr_set(mz2, z2, GMP_RNDN);
      mandelbrot_wucleus(z0x, z0y, dz0x, dz0y, zx, zy, cx, cy, p, epsilon, 64);
      cmpfr_abs2(d2, dz0x, dz0y, t0, t1, GMP_RNDN);
      if (mpfr_less_p(d2, one)) {
        mpfr_printf("local: %Re + %Re i\n", dz0x, dz0y);
        mandelbrot_nucleus(childx, childy, cx, cy, p, 64);
        do {
          mpfr_printf("period: %d\n", p);
          mpfr_printf("nucleus: %Re + %Re i\n", childx, childy);
          p = mandelbrot_parent(angle, rootx, rooty, parentx, parenty, childx, childy, p, 64, epsilon, 64);
          mpfr_printf("root: %Re + %Re i\n", rootx, rooty);
          mpfr_printf("angle: %Qd\n", angle);
          mpfr_set(childx, parentx, GMP_RNDN);
          mpfr_set(childy, parenty, GMP_RNDN);
        } while (p > 0);
        break;
      }
    }
  }
  mpq_clear(angle);
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
  return 0;
}
