#include <stdio.h>
#include <stdlib.h>
#include "mandelbrot_nucleus.h"

void usage(const char *progname) {
  fprintf(stderr,
    "usage: %s bits cx cy period maxiters\n"
    "outputs space separated complex nucleus on stdout\n"
    "example %s 53 -1.75 0 3 100\n",
    progname, progname);
}

int main(int argc, char **argv) {
  if (argc != 6) { usage(argv[0]); return 1; }
  int bits = atoi(argv[1]);
  mpfr_t cx, cy, c0x, c0y;
  mpfr_inits2(bits, cx, cy, c0x, c0y, (mpfr_ptr) 0);
  mpfr_set_str(c0x, argv[2], 10, GMP_RNDN);
  mpfr_set_str(c0y, argv[3], 10, GMP_RNDN);
  int period = atoi(argv[4]);
  int maxiters = atoi(argv[5]);
  mandelbrot_nucleus(cx, cy, c0x, c0y, period, maxiters);
  mpfr_out_str(0, 10, 0, cx, GMP_RNDN);
  putchar(' ');
  mpfr_out_str(0, 10, 0, cy, GMP_RNDN);
  putchar('\n');
  mpfr_clears(cx, cy, c0x, c0y, (mpfr_ptr) 0);
  return 0;
}
