#include <stdio.h>
#include <stdlib.h>
#include "mandelbrot_size_estimate.h"

void usage(const char *progname) {
  fprintf(stderr,
    "usage: %s bits cx cy period\n"
    "outputs space separated complex size on stdout\n"
    "example %s 53 -1.75 0 3\n",
    progname, progname);
}

int main(int argc, char **argv) {
  if (argc != 5) { usage(argv[0]); return 1; }
  int bits = atoi(argv[1]);
  mpfr_t sx, sy, cx, cy;
  mpfr_inits2(bits, sx, sy, cx, cy, (mpfr_ptr) 0);
  mpfr_set_str(cx, argv[2], 10, GMP_RNDN);
  mpfr_set_str(cy, argv[3], 10, GMP_RNDN);
  int period = atoi(argv[4]);
  mandelbrot_size_estimate(sx, sy, cx, cy, period);
  mpfr_out_str(0, 10, 0, sx, GMP_RNDN);
  putchar(' ');
  mpfr_out_str(0, 10, 0, sy, GMP_RNDN);
  putchar('\n');
  mpfr_clears(sx, sy, cx, cy, (mpfr_ptr) 0);
  return 0;
}
