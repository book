#include <stdio.h>
#include <stdlib.h>
#include "mandelbrot_box_period.h"

void usage(const char *progname) {
  fprintf(stderr,
    "usage: %s cx cy radius maxperiod\n"
    "outputs period on stdout, 0 for failure\n"
    "example %s -1.75 0 0.1 1000\n",
    progname, progname);
}

int main(int argc, char **argv) {
  if (argc != 5) { usage(argv[0]); return 1; }
  mpfr_t cx, cy, r, r_log2;
  mpfr_init2(r, 53);
  mpfr_set_str(r, argv[3], 10, GMP_RNDN);
  mpfr_init2(r_log2, 53);
  mpfr_log2(r_log2, r, GMP_RNDN);
  int bits = 16 - mpfr_get_d(r_log2, GMP_RNDN);
  mpfr_init2(cx, bits);
  mpfr_init2(cy, bits);
  mpfr_set_str(cx, argv[1], 10, GMP_RNDN);
  mpfr_set_str(cy, argv[2], 10, GMP_RNDN);
  int maxperiod = atoi(argv[4]);
  int period = mandelbrot_box_period(cx, cy, r, maxperiod);
  printf("%d\n", period);
  mpfr_clears(cx, cy, r, r_log2, (mpfr_ptr) 0);
  return 0;
}
