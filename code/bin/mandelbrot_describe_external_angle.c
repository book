#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include "mandelbrot_binary_angle.h"

void usage(const char *progname) {
  fprintf(stderr, "usage: %s angle\nexample: %s '.01(10)'\nexample: %s '1/12'\n", progname, progname, progname);
}

int main(int argc, char **argv) {
  if (argc != 2) {
    usage(argv[0]);
    return 1;
  }
  struct mandelbrot_binary_angle *b = 0;
  mpq_t q;
  mpq_init(q);
  char *s = 0;
  if (argv[1][0] == '.') {
    b = mandelbrot_binary_angle_from_string(argv[1]);
    if (! b) {
      fprintf(stderr, "parsing binary input failed\n");
      usage(argv[0]);
      return 1;
    }
    struct mandelbrot_binary_angle *b2 = mandelbrot_binary_angle_canonicalize(b);
    if (! b2) {
      fprintf(stderr, "binary input out of bounds\n");
      usage(argv[0]);
      return 1;      
    }
    mandelbrot_binary_angle_delete(b);
    b = b2;
    mandelbrot_binary_angle_to_rational(q, b);
    s = mandelbrot_binary_angle_to_string(b);
  } else {
    int fail = mpq_set_str(q, argv[1], 0);
    if (fail) {
      fprintf(stderr, "parsing rational input failed\n");
      usage(argv[0]);
      return 1;
    }
    mpq_canonicalize(q);
    b = mandelbrot_binary_angle_from_rational(q);
    if (! b) {
      fprintf(stderr, "rational input out of bounds\n");
      usage(argv[0]);
      return 1;
    }
    s = mandelbrot_binary_angle_to_string(b);
  }
  printf("binary: %s\ndecimal: ", s);
  mpq_out_str(0, 10, q);
  printf("\npreperiod: %d\nperiod: %d\n", mandelbrot_binary_angle_preperiod(b), mandelbrot_binary_angle_period(b));
  struct mandelbrot_binary_angle *b2 = mandelbrot_binary_angle_other_representation(b);
  if (b2) {
    char *s2 = mandelbrot_binary_angle_to_string(b2);
    printf("binary (alternative): %s\n", s2);
    free(s2);
    mandelbrot_binary_angle_delete(b2);
  }
  free(s);
  mpq_clear(q);
  mandelbrot_binary_angle_delete(b);
  return 0;
}
