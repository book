diff --git a/.gitignore b/.gitignore
index 2911ab0..8a0fdde 100644
--- a/.gitignore
+++ b/.gitignore
@@ -2 +2,4 @@ render
 colour
+notebook.aux
+notebook.log
+notebook.pdf
diff --git a/notebook.tex b/notebook.tex
new file mode 100644
index 0000000..f3a55cd
--- /dev/null
+++ b/notebook.tex
@@ -0,0 +1,328 @@
+\documentclass[a4paper]{article}
+\usepackage{amsmath}
+\title{Mandelbrot Notebook}
+\author{Claude Heiland-Allen \\
+claude (at) mathr.co.uk}
+\date{\today}
+\begin{document}
+\maketitle
+\section{Newton's Method: Nucleus}
+The nucleus \(n\) of a hyperbolic component of period \(p\) satisfies
+\[F^p(0,n)=0\]
+Applying Newton's method in one complex variable gives
+\[n_{m+1} = n_m - \frac{F^p(0,n_m)}{\frac{\partial}{\partial c} F^p(0,n_m)}\]
+
+\section{Newton's Method: Wucleus}
+A wucleus \(w\) of a point \(c\) within a hyperbolic component of period
+\(p\) satisfies
+\[F^p(w,c)=w\]
+Applying Newton's method in one complex variable gives
+\[w_{m+1} = w_m - \frac{F^p(w_m,c) - w_m}{\frac{\partial}{\partial z} F^p(w_m,c) - 1}\]
+This iteration is unstable because there are \(p\) distinct \(w_i\).
+Perhaps use \(p\)-dimensional Newton's method to find all \(w_i\) at once,
+or trace rays from the orbit of the nucleus to the orbit containing the
+wuclei.
+
+\section{Newton's Method: Bond}
+The bond \(b\) between a hyperbolic component of period \(p\) and nucleus
+\(n\) at internal angle \(\theta\) measured in turns satisfies
+\[\begin{aligned}
+F^p(w,b) &= w \\
+\frac{\partial}{\partial z}F^p(w,b) &= e^{2 \pi i \theta}
+\end{aligned}\]
+Applying Newton's method in two complex variables gives
+\[\left(\begin{matrix}
+\frac{\partial}{\partial z}F^p(w_m,b_m) - 1 &
+\frac{\partial}{\partial c}F^p(w_m,b_m) \\
+\frac{\partial}{\partial z}\frac{\partial}{\partial z}F^p(w_m,b_m) &
+\frac{\partial}{\partial c}\frac{\partial}{\partial z}F^p(w_m,b_m)
+\end{matrix}\right)\left(\begin{matrix}
+w_{m+1} - w_m \\
+b_{m+1} - b_m\end{matrix}\right) = -\left(\begin{matrix}
+F^p(w_m,b_m) - w_m \\
+\frac{\partial}{\partial z}F^p(w_m,b_m) - e^{2 \pi i \theta}
+\end{matrix}\right)\]
+
+\section{Newton's Method: Ray In}
+The next point \(r\) along an external ray with current doubled angle
+\(\theta\), current depth \(p\) and current radius \(R\) satisfies
+\[F^p(0,r)=\lambda R e^{2 \pi i \theta}\]
+where \(\lambda < 1\) controls the sharpness of the ray.
+Applying Newton's method in one complex variable gives
+\[r_{m+1} = r_m - \frac{F^p(0,r_m) - \lambda R e^{2 \pi i \theta}}{\frac{\partial}{\partial c}F^p(0,r_m)}\]
+When crossing dwell bands, double \(\theta\) and increment \(p\),
+resetting the radius \(R\).  Stop tracing when close to the target
+(for example when within the basin of attraction for Newton's method
+for nucleus).
+
+\section{Newton's Method: Ray Out}
+When crossing dwell bands, compute the doubling preimages \(\theta_-\), \(\theta_+\) of \(\theta\)
+\[\begin{aligned}
+\theta_- &= \frac{\theta}{2} \\
+\theta_+ &= \frac{\theta + 1}{2}
+\end{aligned}\]
+Then compute \(r_-\) and \(r_+\) using Newton's method, and choose the
+nearest to \(r_m\) to be \(r_{m+1}\).  Collect a list of which choices
+were made to determine the final \(\theta\) in high precision without
+relying on the bits of \(\theta\) itself.  \(\lambda > 1\) controls the
+sharpness of the ray.  Stop traccing when \(r\) reaches the escape radius.
+
+\section{Newton's Method: Preperiodic}
+A preperiodic point \(u\) with period \(p\) and preperiod \(k\) satisfies
+\[F^{p+k}(0,u) = F^k(0,u)\]
+Applying Newton's method in one complex variable gives
+\[u_{m+1} = u_m - \frac{F^{p+k}(0,u_m) - F^k(0,u_m)}{\frac{\partial}{\partial c} F^{p+k}(0,u_m) - \frac{\partial}{\partial c} F^k(0,u_m)}\]
+However this may converge to a \(u'\) with lower preperiod.  Verify by
+finding the lowest \(k'\) such that Newton's method with preperiod \(k'\)
+starting from \(u'\) still converges to \(u'\) and check that \(k' = k\).
+
+\section{Derivatives Calculation}
+The quadratic polynomial \(F(z,c) = z^2 + c\) and its derivatives under iteration
+\[\begin{aligned}
+F^{m+1}(z,c) &= F(F^m(z,c),c) \\
+\frac{\partial}{\partial z} F^{m+1} &= 2 F^m \frac{\partial}{\partial z} F^m \\
+\frac{\partial}{\partial z} \frac{\partial}{\partial z} F^{m+1} &= 2 F^m \frac{\partial}{\partial z} \frac{\partial}{\partial z} F^m + 2 \left(\frac{\partial}{\partial z} F^m\right)^2 \\
+\frac{\partial}{\partial c} F^{m+1} &= 2 F^m \frac{\partial}{\partial c} F^m + 1\\
+\frac{\partial}{\partial c} \frac{\partial}{\partial z} F^{m+1} &= 2 F^m \frac{\partial}{\partial c} \frac{\partial}{\partial z} F^m + 2 \frac{\partial}{\partial c} F^m \frac{\partial}{\partial z} F^m
+\end{aligned}\]
+with initial values
+\[\begin{aligned}
+F^0(z,c) &= z \\
+\frac{\partial}{\partial z} F^0 &= 1 \\
+\frac{\partial}{\partial z} \frac{\partial}{\partial z} F^0 &= 0 \\
+\frac{\partial}{\partial c} F^0 &= 0 \\
+\frac{\partial}{\partial c} \frac{\partial}{\partial z} F^0 &= 0
+\end{aligned}\]
+
+\section{External Angles: Bulb}
+The \(\frac{p}{q}\) bulb of the period \(1\) cardioid has external angles
+\[
+.(b_0 b_1 \ldots b_{q-3} 0 1) \\
+.(b_0 b_1 \ldots b_{q-3} 1 0)
+\]
+where
+\[ [b_0 b_1 \ldots] = \operatorname{map} (\in (1-\frac{p}{q},1) ) (\operatorname{iterate} (+ \frac{p}{q}) \frac{p}{q}) \]
+
+\section{External Angles: Hub}
+The \(\frac{p}{q}\) bulb has external angles \(.(s_-)\) and \(.(s_+)\).
+The junction point of its hub has external angles in increasing order
+\[\begin{aligned}& .s_-(s_+) \\
+& .s_-(\sigma^\beta s_+) \\
+& \vdots \\
+& .s_-(\sigma^{(q-p-1)\beta} s_+) \\
+& .s_+(\sigma^{(q-p)\beta} s_+) \\
+& \vdots \\
+& .s_+(s_-)
+\end{aligned}\]
+where \(\frac{p}{q}\) has Farey parents
+\[\frac{\alpha}{\beta} < \frac{\gamma}{\delta}\]
+and \(\sigma\) is the shift operator
+\[\sigma^k(b_0 b_1 \ldots) = b_k b_{k+1} \ldots\]
+
+\section{Farey Numbers}
+Given \(\frac{p}{q}\) in lowest terms, it has Farey parents
+\(\frac{\alpha}{\beta} < \frac{\gamma}{\delta}\) where
+\[\begin{aligned}
+\frac{p}{q} &= \frac{\alpha + \gamma}{\beta + \delta} \\
+1 &= p\beta - q\alpha \\
+-1 &= p\delta - q\gamma
+\end{aligned}\]
+Parents can be found by recursively searching from \(\frac{0}{1}\)
+and \(\frac{1}{1}\).
+
+\section{External Angles: Tuning}
+Given an external angle pair corresponding to a hyperbolic component
+\[\begin{aligned}
+s_- &= .(a_0 a_1 \ldots ) \\
+s_+ &= .(b_0 b_1 \ldots )
+\end{aligned}\]
+and an external angle
+\[t = .c_0 c_1 \ldots (d_0 d_1 \ldots )\]
+then \(t\) tuned by \(s\) is formed by replacing every \(0\) in \(t\)
+by \(s_-\) and every \(1\) in \(t\) by \(s_+\) as finite blocks being
+the repeated part of each.
+
+\section{External Angles: Tips}
+Given hub angles \(a_0, a_1, \ldots a_{m-1}\) the tip of the spoke
+between \(a_i\) and \(a_{i+1}\) has external angle with binary
+representation the longest matching prefix of \(a_i\) and \(a_{i+1}\)
+with \(1\) appended.  The widest spoke has the shortest binary
+representation.
+
+\section{Translating Hubs Towards Tips}
+Translating a hub towards the tip of the widest spoke repeats the
+last preperiodic digit.  Translating towards the tip of any other
+spoke replaces the preperiodic
+part with a modified tip: the final \(1\) becomes \(01\) for angles
+below the widest tip and the final \(1\) becomes \(10\) for angles
+above the widest tip.
+
+\section{Islands In The Spokes}
+The largest (lowest period) island after a hub is in the widest spoke.
+Its external angles are the repetitition of the first \(p\) (its period)
+digits of the external angles of the widest spoke.
+
+Numbering spokes in decreasing order of width with the widest spoke
+numbered \(1\), the angled internal address for the path
+\[ \frac{p}{q}*s_0 s_1 \ldots s_m\]
+is
+\[ 1 \frac{p}{q} (q + \sum_{i=0}^0 {s_i}) (q + \sum_{i=0}^1 {s_i}) \ldots (q + \sum_{i=0}^m {s_i})\]
+
+\section{Islands In The Hairs}
+An island with period \(p > 1\) is surrounded by hairs.  There are
+\(2^n\) lowest period islands in the \(2^n\) hairs at depth \(n\) and
+offset \(k\), each with period
+\[q = n p + k\]
+Their external angles are formed by counting in binary using the external
+angles of the parent period \(p\) island as digits
+\[e = .(\pm_0 \pm_1 \pm_2 \ldots \pm_{n-1} 0) \text{ for } k = 1\]
+where the external angles of \(p\) are
+\[\begin{aligned}
++ &= .(+_0 +_1 \ldots +_{p-1}) \\
+- &= .(-_0 -_1 \ldots -_{p-1})
+\end{aligned}\]
+
+\section{Islands In Embedded Julias}
+An island of period \(q\) in the hairs of an island of period \(p\)
+is surrounded by an embedded Julia set.  If the external angles of
+\(p\) are \(.(+)\) and \(.(-)\) and an external angle of \(q\) is
+\(.(\ldots 0)\) then the external angles heading outwards along the
+hair within the embedded Julia set are
+\[.(\ldots 0 [+])\]
+and heading inwards along the hair are
+\[.(\ldots 0 [-])\]
+Mixing inwards and outwards motions gives
+\[\text{TODO}\]
+
+\section{Multiply Embedded Julias}
+TODO diagram
+
+\section{Continuous Iteration Count}
+If \(n\) is the lowest such that
+\[|z_n| > R >> 2\]
+then
+\[v = n - \log_2 \frac{\log |z_n|}{\log R}\]
+where the value subtracted is in \([0,1)\).  When combined with 
+the final angle \(\theta = \arg z_n = \tan^{-1}\frac{\Im(z_n)}{\Re(z_n)}\)
+these can be used to tile the exterior with hyperbolic squares.
+
+TODO diagram
+
+The local coordinates of \(x\) are
+\[(v \mod 1, \frac{\theta}{2 \pi} \mod 1)\]
+
+\section{Interior Coordinates}
+The interior coordinate \(b\) for a point \(c\) within a hyperbolic
+component of period \(p\) can be found by applying Newton's method in
+one complex variable to find a wucleus \(w\) for \(c\) satisfying
+\[F^p(w,c)=w\]
+then
+\[b = \frac{\partial}{\partial z} F^p(w,c)\]
+The interior coordinate satisfies
+\[|b| < 1\]
+and can be used to map a disc into the interior of a component.
+
+\section{Exterior Distance}
+Given \(c\) outside the Mandelbrot set, the exterior distance estimate
+\[d = \lim_{n \to \infty} 2 \frac{|F^n(0,c)| \log|F^n(0,c)|}{|\frac{\partial}{\partial c}F^n(0,c)|}\]
+satisfies by the Koebe \(\frac{1}{4}\) Theorem
+\[\forall c' . |c - c'| < \frac{d}{4} \implies c' \notin M\]
+Shading using \(t = \tanh \frac{d}{\text{pixel size}}\) works well
+because \(t \in [0,1)\) and \(\tanh 4 \approx 1\).
+
+\section{Interior Distance}
+Given \(c\) inside a hyperbolic component of period \(p\), with a wucleus
+\(w\), then the interior distance estimate
+\[d = \frac{1 - |\frac{\partial}{\partial z}F^p(w,c)|^2}{\left|\frac{\partial}{\partial c}\frac{\partial}{\partial z}F^p(w,c) + \frac{\frac{\partial}{\partial z}\frac{\partial}{\partial z}F^p(w,c) \frac{\partial}{\partial c} F^p(w,c)}{1 - \frac{\partial}{\partial z} F^p(w,c)}\right|}\]
+satisfies by the Koebe \(\frac{1}{4}\) Theorem
+\[\forall c' . |c - c'| < \frac{d}{4} \implies c' \in M\]
+Shading using \(t = \tanh \frac{d}{\text{pixel size}}\) works well
+because \(t \in [0,1)\) and \(\tanh 4 \approx 1\).
+
+\section{Perturbation}
+TODO
+
+\section{Finding Atoms}
+Given an angled internal address, the nucleus and size of an atom can
+be found by:
+\begin{itemize}
+\item splitting the address to island and children
+\item finding the external angles of the island
+\item tracing the external ray towards the island
+\item using Newton's method to find the nucleus of the island
+\end{itemize}
+and then iteratively for the children
+\begin{itemize}
+\item find the bond at the internal angle of the child
+\item using the size estimate and normal at the bond point to estimate the nucleus
+\item using Newton's method to find the nucleus of the child
+\end{itemize}
+
+\section{Child Size Estimates}
+For the \(\frac{q}{p}\) child of a cardioid of size \(R\):
+\[ r \approx \frac{R}{p^2} \sin\left(2 \pi \frac{q}{p}\right) \]
+For the \(\frac{q}{p}\) child of a disc of size \(R\):
+\[ r \approx \frac{R}{p^2} \]
+The size of a cardioid is approximated by the distance from its cusp
+to its \(\frac{1}{2}\) bond, and for a disc its radius.
+The nucleus \(n\) of the child at internal angle \(\theta\) with bond \(b\)
+is approximated by
+\[n \approx b - i r \frac{\partial \theta}{\partial b}\]
+For the main period \(1\) cardioid
+\[b = u (1 - u)\]
+where
+\[u = \frac{e^{2 \pi i \frac{q}{p}}}{2} \]
+
+\section{Atom Shape Estimates}
+Given the nucleus \(c\) and period \(p\) of a hyperbolic component,
+the shape discriminant \(\epsilon_p\) satisfies
+\[\epsilon_p = - \frac{1}{(\frac{\partial}{\partial c})(\frac{\partial}{\partial z})} \left(\frac{\frac{\partial}{\partial c}\frac{\partial}{\partial c}}{2\frac{\partial}{\partial c}} + \frac{\frac{\partial}{\partial c}\frac{\partial}{\partial z}}{\frac{\partial}{\partial z}}\right)\]
+where the derivatives are evaluated at \(F^p(c,c)\).
+Then \(\epsilon_p \approx 0\) for cardioids and \(\epsilon_p \approx 1\) for discs.
+
+\section{Atom Size Estimates}
+Given nucleus \(c\) and period \(p\), calculate \(\gamma\) by
+\[\begin{aligned}
+z_0 &= 0 \\
+z_{m+1} &= z_m^2 + c \\
+\lambda &= \prod_{i=0}^{p-1} 2 z_i \\
+\beta &= \sum_{i=0}^{p-1} \frac{1}{\prod_{j=1}^i 2 z_i} \\
+\gamma &= \frac{1}{\beta \lambda^2}
+\end{aligned}\]
+then the size estimate is \(|\gamma|\) and the orientation estimate is
+\(\arg \gamma\).
+
+\section{Tracing Equipotentials}
+Proceed as external ray tracing, but keep dwell and radius fixed and
+increment the angle by \(\frac{2 \pi}{\text{sharpness}}\).
+
+\section{Buddhabrot}
+For each exterior \(c\), colour hue according to dwell and plot the \(z\)
+iterates into an accumulation buffer.  Post-process this high dynamic
+range image to map brightness to point density.
+
+\section{Atom Domain Size Estimate}
+Given nucleus \(c\) with period \(p\), find \(1 \le q < p\) such that
+\(|F^q(0,c)|\) is minimized.  Then the atom domain size estimate is
+\[R = \frac{F^q(0,c)}{\frac{\partial}{\partial c}F^p(0,c)}\]
+
+\section{Stretching Cusps}
+Moebius transformation:
+\[\begin{aligned}
+f(z) &= \frac{ a z + b } { c z + d } \\
+f^{-1}(z) &= \frac{ d z - b } {-c z + a}
+\end{aligned}\]
+Unwrapping circle to line:
+\[g(z) = \frac{(z - p_0)(p_1 - p_\infty)}{(z-p_\infty)(p_1 - p_0)}\]
+Mapping cardioids to circles:
+\[\begin{aligned}
+h(z) &= \sqrt{1 - 4 z} - 1 \\
+h^{-1}(z) &= \frac{1 - (z+1)^2}{4}
+\end{aligned}\]
+Derivatives for distance estimation:
+\[\begin{aligned}
+\frac{\partial}{\partial z} \frac{a z + b}{c z + d} &= \frac{ad - bc}{(c z + d)^2} \\
+\frac{\partial}{\partial z} \frac{1 - (z+1)^2}{4} &= - \frac{z + 1}{2}
+\end{aligned}\]
+
+\end{document}
